package com.dt.app.config;

import com.dt.app.api.sys.service.BaseConfigService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

/**
 * 初始化Application事件监听器
 * @author DT
 * @date 2021/10/28 19:23
 */
@Configuration
@CommonsLog
public class EventListenerConfig implements ApplicationListener<ApplicationReadyEvent> {

    private final BaseConfigService baseConfigService;

    public EventListenerConfig(BaseConfigService baseConfigService) {
        this.baseConfigService = baseConfigService;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        log.info("application listener init....");
        baseConfigService.initConfigData();
    }
}
