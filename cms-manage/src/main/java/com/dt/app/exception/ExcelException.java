package com.dt.app.exception;

import org.springframework.security.core.AuthenticationException;

import java.io.Serializable;

/**
 * 导出Excel表格异常
 * @author DT
 * @date 2021/10/24 1:55
 */
public class ExcelException extends AuthenticationException implements Serializable {

    private static final long serialVersionUID = -7269804134178047586L;

    public ExcelException(String msg) {
        super(msg);
    }
}
