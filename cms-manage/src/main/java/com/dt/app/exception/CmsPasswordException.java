package com.dt.app.exception;

import org.springframework.security.core.AuthenticationException;

import java.io.Serializable;

/**
 * 密码错误异常处理
 * @author DT
 * @date 2021/10/9 22:10
 */
public class CmsPasswordException extends AuthenticationException implements Serializable {

    private static final long serialVersionUID = -7126856407406379770L;

    public CmsPasswordException(String msg) {
        super(msg);
    }
}
