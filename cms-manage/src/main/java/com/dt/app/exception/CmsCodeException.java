package com.dt.app.exception;

import org.springframework.security.core.AuthenticationException;

import java.io.Serializable;

/**
 * 验证码验证失败异常
 * @author DT
 * @date 2021/6/9 21:14
 */
public class CmsCodeException extends AuthenticationException implements Serializable {

    private static final long serialVersionUID = -1992206615098107874L;

    public CmsCodeException(String msg) {
        super(msg);
    }
}
