package com.dt.app.exception;

import org.springframework.security.core.AuthenticationException;

import java.io.Serializable;

/**
 * Token验证失败异常
 * @author DT
 * @date 2021/6/5 12:18
 */
public class CmsTokenException extends AuthenticationException implements Serializable {

    private static final long serialVersionUID = -7715850416006405662L;

    public CmsTokenException(String msg) {
        super(msg);
    }
}
