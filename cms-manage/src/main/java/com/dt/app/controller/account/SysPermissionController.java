package com.dt.app.controller.account;

import com.dt.app.api.sys.service.SysPermissionService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.modules.sys.entities.SysPermissionEntity;
import com.dt.app.modules.sys.vo.request.SysMenuRequest;
import com.dt.app.modules.sys.vo.request.SysMenuSaveRequest;
import com.dt.app.modules.sys.vo.response.SysMenuTreeResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author DT
 * @date 2021/6/5 0:33
 */
@Api(tags = "系统菜单权限信息API")
@RestController
@RequestMapping("api/v1/permission")
public class SysPermissionController {

    private final SysPermissionService sysPermissionService;

    public SysPermissionController(SysPermissionService sysPermissionService) {
        this.sysPermissionService = sysPermissionService;
    }

    @ApiOperation(value = "查询菜单树列表")
    @GetMapping("list")
    public ResultUtil<List<SysMenuTreeResponse>> list(SysMenuRequest request){
        return sysPermissionService.queryList(request);
    }

    @ApiOperation(value = "获取所有权限数据")
    @GetMapping("listTree")
    public ResultUtil<List<SysMenuTreeResponse>> listTree() {
        return sysPermissionService.listTree();
    }

    @PostMapping("save")
    @ApiOperation(value = "添加系统菜单")
    public ResultUtil<SysMenuSaveRequest> save(@RequestBody @Valid SysMenuSaveRequest request){
        return sysPermissionService.saveMenu(request);
    }

    @GetMapping("getById/{id}")
    @ApiOperation(value = "根据id查询菜单")
    public ResultUtil<SysPermissionEntity> getById(@PathVariable Long id){
        return sysPermissionService.getMenuById(id);
    }

    @PutMapping("update")
    @ApiOperation(value = "修改系统菜单")
    public ResultUtil<SysMenuSaveRequest> update(@RequestBody @Valid SysMenuSaveRequest request){
        return sysPermissionService.updateMenu(request);
    }

    @DeleteMapping("delete/{id}")
    @ApiOperation(value = "删除系统菜单")
    public ResultUtil<SysPermissionEntity> delete(@PathVariable Long id){
        return sysPermissionService.deleteMenu(id);
    }
}
