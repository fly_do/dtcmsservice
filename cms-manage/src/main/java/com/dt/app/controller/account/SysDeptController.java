package com.dt.app.controller.account;

import com.dt.app.api.sys.service.SysDeptService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.modules.sys.entities.SysDepartmentEntity;
import com.dt.app.modules.sys.vo.request.SysDeptRequest;
import com.dt.app.modules.sys.vo.request.SysDeptSaveRequest;
import com.dt.app.modules.sys.vo.response.SysDeptTreeResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author DT
 * @date 2021/6/13 12:02
 */
@Api(tags = "系统部门信息API")
@RestController
@RequestMapping("api/v1/dept")
public class SysDeptController {

    private final SysDeptService sysDeptService;

    public SysDeptController(SysDeptService sysDeptService) {
        this.sysDeptService = sysDeptService;
    }

    @ApiOperation(value = "查询部门树列表")
    @GetMapping("list")
    public ResultUtil<List<SysDeptTreeResponse>> list(SysDeptRequest request){
        return sysDeptService.queryList(request);
    }

    @ApiOperation(value = "获取所有部门树数据")
    @GetMapping("listTree/{type}")
    public ResultUtil<List<SysDeptTreeResponse>> listTree(@PathVariable Integer type) {
        return sysDeptService.listTree(type);
    }

    @PostMapping("save")
    @ApiOperation(value = "添加系统部门")
    public ResultUtil<SysDeptSaveRequest> save(@RequestBody @Valid SysDeptSaveRequest request){
        return sysDeptService.saveDept(request);
    }

    @GetMapping("getById/{id}")
    @ApiOperation(value = "根据id查询部门")
    public ResultUtil<SysDepartmentEntity> getById(@PathVariable String id){
        return sysDeptService.getDeptById(id);
    }

    @PutMapping("update")
    @ApiOperation(value = "修改系统部门")
    public ResultUtil<SysDeptSaveRequest> update(@RequestBody @Valid SysDeptSaveRequest request){
        return sysDeptService.updateDept(request);
    }

    @DeleteMapping("delete/{id}")
    @ApiOperation(value = "删除系统部门")
    public ResultUtil<SysDepartmentEntity> delete(@PathVariable String id){
        return sysDeptService.deleteDept(id);
    }

}
