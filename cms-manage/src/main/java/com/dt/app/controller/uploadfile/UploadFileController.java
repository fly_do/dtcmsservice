package com.dt.app.controller.uploadfile;

import com.dt.app.api.sys.service.UploadFileService;
import com.dt.app.common.response.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author DT
 * @date 2021/11/5 18:14
 */
@Api(tags = "文件上传API")
@RestController
@RequestMapping("api/v1/file")
public class UploadFileController {

    private final UploadFileService uploadFileService;

    public UploadFileController(UploadFileService uploadFileService) {
        this.uploadFileService = uploadFileService;
    }

    @ApiOperation(value = "判断Bucket是否存在")
    @PostMapping("/bucketIsExist")
    public ResultUtil<?> bucketIsExist(@RequestParam String bucketName) {
        return uploadFileService.bucketIsExist(bucketName);
    }

    @ApiOperation(value = "创建Bucket")
    @PostMapping("/createBucket")
    public ResultUtil<?> createBucket(@RequestParam String bucketName) {
        return uploadFileService.createBucket(bucketName);
    }

    @ApiOperation(value = "创建Bucket策略桶")
    @PostMapping("/createBucketPolicy")
    public ResultUtil<?> createBucketPolicy(@RequestParam String bucketName) {
        return uploadFileService.createBucketPolicy(bucketName);
    }

    @ApiOperation(value = "删除Bucket")
    @PostMapping("/removeBucket")
    public ResultUtil<?> removeBucket(@RequestParam String bucketName) {
        return uploadFileService.removeBucket(bucketName);
    }

    @ApiOperation(value = "上传文件")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "bucket名称",name = "bucketName",required = true),
            @ApiImplicitParam(value = "上传文件",name = "file",required = true,allowMultiple = true,dataType = "MultipartFile")
    })
    @PostMapping(value = "/uploadFile",headers = "content-type=multipart/form-data")
    public ResultUtil<?> uploadFile(@RequestParam(value = "file") MultipartFile file, @RequestParam String bucketName) {
        return uploadFileService.uploadFile(file, bucketName);
    }

    @ApiOperation(value = "下载文件")
    @PostMapping("/downloadFile")
    public void downloadFile(@RequestParam String bucketName, @RequestParam String fileName, HttpServletResponse response) {
        uploadFileService.downloadFile(bucketName, fileName,response);
    }

    @ApiOperation(value = "删除文件")
    @PostMapping("/delFile")
    public ResultUtil<?> delFile(@RequestParam String bucketName, @RequestParam String fileName) {
        return uploadFileService.delFile(bucketName, fileName);
    }

    @ApiOperation(value = "上传头像")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "上传文件",name = "file",required = true,allowMultiple = true,dataType = "MultipartFile")
    })
    @PostMapping(value = "/uploadCover",headers = "content-type=multipart/form-data")
    public ResultUtil<Map<String, Object>> uploadCover(@RequestParam(value = "file") MultipartFile file) {
        return uploadFileService.uploadCover(file);
    }
}
