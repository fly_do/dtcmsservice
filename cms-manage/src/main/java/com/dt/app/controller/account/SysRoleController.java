package com.dt.app.controller.account;

import com.dt.app.api.sys.service.SysRoleService;
import com.dt.app.common.constant.LogTypeCode;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.syslog.Log;
import com.dt.app.modules.sys.entities.SysRoleEntity;
import com.dt.app.modules.sys.vo.request.SysRoleRequest;
import com.dt.app.modules.sys.vo.request.SysRoleSaveRequest;
import com.dt.app.modules.sys.vo.response.SysRoleResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author DT
 * @date 2021/6/5 0:32
 */
@Api(tags = "系统角色信息API")
@RestController
@RequestMapping("api/v1/role")
public class SysRoleController {

    private final SysRoleService sysRoleService;

    public SysRoleController(SysRoleService sysRoleService) {
        this.sysRoleService = sysRoleService;
    }

    @ApiOperation(value = "查询角色权限列表")
    @GetMapping("list")
    public ResultUtil<List<SysRoleResponse>> list(SysRoleRequest request){
        return sysRoleService.queryList(request);
    }

    @Log(title = "新增平台系统角色", businessType = LogTypeCode.INSERT)
    @PostMapping("save")
    @ApiOperation(value = "添加系统角色")
    public ResultUtil<SysRoleSaveRequest> save(@RequestBody @Valid SysRoleSaveRequest request){
        return sysRoleService.saveRole(request);
    }

    @GetMapping("getById/{id}")
    @ApiOperation(value = "根据id查询角色")
    public ResultUtil<SysRoleEntity> getById(@PathVariable Integer id){
        return sysRoleService.getRoleById(id);
    }

    @Log(title = "更新平台系统角色", businessType = LogTypeCode.UPDATE)
    @PutMapping("update")
    @ApiOperation(value = "修改系统角色")
    public ResultUtil<SysRoleSaveRequest> update(@RequestBody @Valid SysRoleSaveRequest request){
        return sysRoleService.updateRole(request);
    }

    @Log(title = "删除平台系统角色", businessType = LogTypeCode.DELETE)
    @DeleteMapping("delete/{id}")
    @ApiOperation(value = "删除系统角色")
    public ResultUtil<SysRoleEntity> delete(@PathVariable Integer id){
        return sysRoleService.delete(id);
    }

    @GetMapping("findAll")
    @ApiOperation(value = "查询所有角色")
    public ResultUtil<List<SysRoleEntity>> findAll(){
        return sysRoleService.findAll();
    }
}
