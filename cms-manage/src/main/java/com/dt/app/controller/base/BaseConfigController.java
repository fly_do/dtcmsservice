package com.dt.app.controller.base;

import com.alibaba.fastjson.JSONObject;
import com.dt.app.api.sys.service.BaseConfigService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.modules.config.entities.BaseConfigEntity;
import com.dt.app.modules.sys.entities.SysUserEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author DT
 * @date 2021/10/29 23:25
 */
@Api(tags = "系统参数配置API")
@RestController
@RequestMapping("api/v1/config")
public class BaseConfigController {

    private final BaseConfigService baseConfigService;

    public BaseConfigController(BaseConfigService baseConfigService) {
        this.baseConfigService = baseConfigService;
    }

    @ApiOperation(value = "获取系统初始化数据")
    @GetMapping(value = "init")
    public ResultUtil<JSONObject> init() {
        return baseConfigService.getInitData();
    }

    @ApiOperation(value = "获取主题列表")
    @GetMapping(value = "queryThemeData")
    public ResultUtil<List<BaseConfigEntity>> queryThemeData() {
        return baseConfigService.queryThemeList();
    }

    @ApiOperation(value = "启用主题")
    @PostMapping(value = "updateThemeEnabled/{id}")
    public ResultUtil<BaseConfigEntity> updateThemeEnabled(@PathVariable String id) {
        return baseConfigService.updateThemeEnabled(id);
    }

    @ApiOperation(value = "获取系统分类数据")
    @GetMapping(value = "listCategory")
    public ResultUtil<?> listCategory() {
        return baseConfigService.listCategory();
    }

    @PostMapping("save")
    @ApiOperation(value = "添加系统参数配置")
    public ResultUtil<BaseConfigEntity> save(@RequestBody @Valid BaseConfigEntity request){
        return baseConfigService.saveConfig(request);
    }

    @PutMapping("update")
    @ApiOperation(value = "更新系统参数配置")
    public ResultUtil<BaseConfigEntity> update(@RequestBody @Valid BaseConfigEntity request){
        return baseConfigService.updateConfig(request);
    }

    @DeleteMapping("delete/{id}")
    @ApiOperation(value = "删除配置项")
    public ResultUtil<BaseConfigEntity> delete(@PathVariable String id){
        return baseConfigService.deleteConfig(id);
    }

    @ApiOperation(value = "查询参数配置列表")
    @GetMapping("list")
    public ResultUtil<List<BaseConfigEntity>> list(BaseConfigEntity request){
        return baseConfigService.queryList(request);
    }

}
