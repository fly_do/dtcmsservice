package com.dt.app.controller.account;

import com.dt.app.api.sys.service.SysRolePermissionService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.modules.sys.vo.request.SysRoleMenuRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author DT
 * @date 2021/6/20 14:48
 */
@Api(tags = "系统角色关联菜单权限信息API")
@RestController
@RequestMapping("api/v1/role-permission")
public class SysRolePermissionController {

    private final SysRolePermissionService sysRolePermissionService;

    public SysRolePermissionController(SysRolePermissionService sysRolePermissionService) {
        this.sysRolePermissionService = sysRolePermissionService;
    }

    @PostMapping("saveRoleMenu")
    @ApiOperation("角色权限分配")
    public ResultUtil<?> saveRoleMenu(@RequestBody SysRoleMenuRequest request){
        if(!request.getIds().isEmpty()){
            sysRolePermissionService.saveRoleMenu(request.getRoleId(),request.getIds());
            return ResultUtil.success();
        }else{
            return ResultUtil.error();
        }
    }
}
