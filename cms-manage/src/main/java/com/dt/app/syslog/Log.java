package com.dt.app.syslog;

import com.dt.app.common.constant.LogTypeCode;

import java.lang.annotation.*;

/**
 * 日志注解自定义
 * ElementType.TYPE:用于描述类、接口(包括注解类型) 或enum声明
 * ElementType.METHOD:用于描述方法
 * @author DT
 * @date 2021/11/20 15:18
 */
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Log {

    /**
     * 日志操作模块名称
     */
    String title() default "";

    /**
     * 日志处理模块类型（新增、删除、更新）
     */
    String businessType() default LogTypeCode.OTHER;

}
