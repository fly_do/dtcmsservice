package com.dt.app;

import com.dt.app.common.uploadfile.minio.MinIoUploadFile;
import com.dt.app.common.utils.CmsUtils;
import com.dt.app.common.utils.PropertyUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 后台启动器
 * @author DT
 * @date 2021/6/2 21:37
 */
@SpringBootApplication
@EnableAsync
@MapperScan(basePackages = {"com.dt.app.api.*.mapper"})
@Import(value = { MinIoUploadFile.class, PropertyUtils.class})
public class ManageApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ManageApplication.class);
        app.setBannerMode(Banner.Mode.LOG);
        app.run(args);
    }

    @Bean("sysTaskExecutor")
    public ThreadPoolTaskExecutor executor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 此方法返回可用处理器的虚拟机的最大数量; 不小于1
        int core = Runtime.getRuntime().availableProcessors();
        CmsUtils.LOG.info("======>>>处理器数量:"+core);
        // 设置核心线程数
        executor.setCorePoolSize(16);
        // 设置最大线程数
        executor.setMaxPoolSize(16);
        // 除核心线程外的线程存活时间
        executor.setKeepAliveSeconds(3);
        // 如果传入值大于0，底层队列使用的是LinkedBlockingQueue,否则默认使用SynchronousQueue
        executor.setQueueCapacity(40);
        // 线程名称前缀
        executor.setThreadNamePrefix("cms-pool-");
        // 设置拒绝策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return executor;
    }

}
