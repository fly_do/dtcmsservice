package com.dt.app;

import com.dt.app.common.constant.ConstantCode;
import com.dt.app.common.service.RedisService;
import com.dt.app.modules.sys.entities.SysUserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

/**
 * @author DT
 * @date 2021/6/5 19:07
 */
@SpringBootTest
public class CmsApplicationTest {

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RedisService redisService;

    @Test
    public void test(){
        System.out.println(passwordEncoder.encode("admin"));
    }

    @Test
    void set() {
        SysUserEntity userEntity = new SysUserEntity();
        userEntity.setId(1L);
        userEntity.setUsername("admin");
        userEntity.setPassword(passwordEncoder.encode("admin"));
        userEntity.setAccountName("DT辰白");
        redisService.set(ConstantCode.USER_KEY + userEntity.getUsername(),userEntity);
        // 取值
        SysUserEntity object = (SysUserEntity) redisService.get(ConstantCode.USER_KEY + "admin");
        System.out.println("获取key==============");
        System.out.println(object.getUsername());
    }

    @Test
    void setList() {
        List<SysUserEntity> sysUserEntityList = new ArrayList<>();
        SysUserEntity userEntity = new SysUserEntity();
        userEntity.setId(1L);
        userEntity.setUsername("admin");
        userEntity.setPassword(passwordEncoder.encode("admin"));
        userEntity.setAccountName("DT辰白");
        SysUserEntity userEntity2 = new SysUserEntity();
        userEntity2.setId(2L);
        userEntity2.setUsername("root");
        userEntity2.setPassword(passwordEncoder.encode("root"));
        userEntity2.setAccountName("DT小白");
        sysUserEntityList.add(userEntity);
        sysUserEntityList.add(userEntity2);
        redisService.lPush(ConstantCode.PERMISSION_KEY + 2,sysUserEntityList);
        // 取值
        List<SysUserEntity> objectList = (List<SysUserEntity>)(List)redisService.lRange(ConstantCode.PERMISSION_KEY + 2, 0, -1);
        System.out.println("获取key==============");
        System.out.println(objectList);
    }

    /**
     * 把List<Object[]>转换成List<T>
     */
    public static <T> List<T> objectToBean(List<Object[]> objList, Class<T> clz) throws Exception{
        if (objList==null || objList.size()==0) {
            return null;
        }
        Class<?>[] cz = null;
        Constructor<?>[] cons = clz.getConstructors();
        for (Constructor<?> ct : cons) {
            Class<?>[] clazz = ct.getParameterTypes();
            if (objList.get(0).length == clazz.length) {
                cz = clazz;
                break;
            }
        }
        List<T> list = new ArrayList<>();
        for (Object[] obj : objList) {
            Constructor<T> cr = clz.getConstructor(cz);
            list.add(cr.newInstance(obj));
        }
        return list;
    }

    @Test
    void lIndex() {
        Object index = redisService.lIndex(ConstantCode.PERMISSION_KEY + 1, 0);
        Long size = redisService.lSize(ConstantCode.PERMISSION_KEY + 1);
        System.out.println("根据索引获取List中的属性===============");
        System.out.println(size);
        System.out.println(index);
    }

    @Test
    void del() {
        Boolean del = redisService.del(ConstantCode.PERMISSION_KEY + 1);
        System.out.println("删除key===============");
        System.out.println(del);
    }

    @Test
    void setKeyTime() {
        SysUserEntity userEntity = new SysUserEntity();
        userEntity.setId(1L);
        userEntity.setUsername("admin");
        userEntity.setPassword(passwordEncoder.encode("admin"));
        userEntity.setAccountName("DT辰白");
        // 设置过期时间3分钟
        redisService.set(ConstantCode.USER_KEY + userEntity.getUsername(),userEntity, 60 * 3);
        // 取值
        Object object = redisService.get(ConstantCode.USER_KEY + "admin");
        System.out.println("获取key==============");
        System.out.println(object);
    }

    @Test
    void getExpire() {
        Long expire = redisService.getExpire(ConstantCode.USER_KEY + "admin");
        System.out.println("获取过期时间==============");
        System.out.println(expire);
    }



}
