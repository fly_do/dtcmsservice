/*
 Navicat Premium Data Transfer

 Source Server         : DT
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : 127.0.0.1:3306
 Source Schema         : dtcms

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 20/11/2021 23:07:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_config
-- ----------------------------
DROP TABLE IF EXISTS `base_config`;
CREATE TABLE `base_config`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数类型',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数名称',
  `k` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'key',
  `v` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'value',
  `enabled` tinyint(1) NULL DEFAULT 0 COMMENT '是否启用（1：是，0否）',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_config
-- ----------------------------
INSERT INTO `base_config` VALUES ('1000000066909420', '系统主题配置', '主题七', '主题七', 'theme_seven.css', 1, '2021-11-19 23:57:38', '2021-11-20 00:35:57');
INSERT INTO `base_config` VALUES ('1000000256170896', '系统主题配置', '主题二', '主题二', 'theme_two.css', 0, '2021-10-31 18:27:53', '2021-11-19 23:57:49');
INSERT INTO `base_config` VALUES ('1000000332864445', '系统主题配置', '主题四', '主题四', 'theme_four.css', 0, '2021-10-31 18:27:53', '2021-11-20 00:26:02');
INSERT INTO `base_config` VALUES ('1000000470437159', '首页链接入口', '用户管理', 'usermannage', '/system/user/UserList', 0, '2021-11-06 12:12:59', '2021-11-06 12:13:41');
INSERT INTO `base_config` VALUES ('1000000682943458', '系统主题配置', '主题五', '主题五', 'theme_five.css', 0, '2021-10-31 18:27:53', '2021-11-20 00:26:12');
INSERT INTO `base_config` VALUES ('1000001361614097', '系统主题配置', '主题六', '主题六', 'theme_six.css', 0, '2021-10-31 18:27:53', '2021-11-20 00:35:57');
INSERT INTO `base_config` VALUES ('1000001698990134', '系统主题配置', '主题一', '主题一', 'theme_one.css', 0, '2021-10-31 18:27:53', '2021-11-20 00:26:25');
INSERT INTO `base_config` VALUES ('1000001739126234', '系统主题配置', '主题三', '主题三', 'theme_three.css', 0, '2021-10-31 18:27:53', '2021-11-19 18:51:56');

-- ----------------------------
-- Table structure for sys_department
-- ----------------------------
DROP TABLE IF EXISTS `sys_department`;
CREATE TABLE `sys_department`  (
  `id` varchar(56) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '部门id',
  `pid` varchar(56) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '上级部门id',
  `parent_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '上级部门名称',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '部门名称',
  `order_num` int NULL DEFAULT NULL COMMENT '序号',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_department
-- ----------------------------
INSERT INTO `sys_department` VALUES ('1000000130649867', '1000000874136172', '后端开发', 'PHP开发部', 2, '2021-06-20 21:55:35', '2021-06-20 21:55:35');
INSERT INTO `sys_department` VALUES ('1000000174227147', '1000001620535597', '客服部', '市场部门', 1, '2021-06-17 21:33:41', '2021-06-17 21:34:03');
INSERT INTO `sys_department` VALUES ('1000000187669045', '1000000874136172', '后端开发', 'Python开发部', 0, '2021-06-20 21:58:41', '2021-06-20 21:58:41');
INSERT INTO `sys_department` VALUES ('1000000204663981', '1000001200689941', '技术部', '前端部门', 3, '2021-06-17 21:27:59', '2021-06-17 21:28:22');
INSERT INTO `sys_department` VALUES ('1000000354532983', '0', '根节点', 'DT设计科技有限公司', 2, '2021-06-20 21:53:26', '2021-06-20 21:53:26');
INSERT INTO `sys_department` VALUES ('1000000622591924', '1000001620535597', '客服部', '设计部', 1, '2021-06-20 21:53:49', '2021-06-20 21:57:45');
INSERT INTO `sys_department` VALUES ('1000000819493000', '1000000874136172', '后端开发', 'C语言开发部', 3, '2021-06-20 21:55:49', '2021-06-20 21:55:49');
INSERT INTO `sys_department` VALUES ('1000000874136172', '1000002112552988', '研发部', '后端开发', 1, '2021-06-20 21:55:09', '2021-06-20 21:55:09');
INSERT INTO `sys_department` VALUES ('1000000952846438', '1000001251633881', '财务部', '财务部门', 1, '2021-06-17 21:29:00', '2021-10-03 16:50:39');
INSERT INTO `sys_department` VALUES ('1000001186458564', '1000001637526739', '服务部门', '服务二部', 2, '2021-06-20 21:56:29', '2021-06-20 21:56:29');
INSERT INTO `sys_department` VALUES ('1000001200689941', '1000001776185099', 'DT编程科技有限公司', '技术部', 1, '2021-06-13 19:15:04', '2021-06-17 21:25:31');
INSERT INTO `sys_department` VALUES ('1000001218109551', '1000000874136172', '后端开发', 'JAVA开发部', 1, '2021-06-20 21:55:24', '2021-06-20 21:55:24');
INSERT INTO `sys_department` VALUES ('1000001251633881', '1000001776185099', 'DT编程科技有限公司', '财务部', 2, '2021-06-13 14:35:36', '2021-06-17 21:29:25');
INSERT INTO `sys_department` VALUES ('1000001258096779', '1000001200689941', '技术部', 'UI设计部门', 4, '2021-06-17 21:28:10', '2021-06-17 21:31:41');
INSERT INTO `sys_department` VALUES ('1000001341234088', '1000001776185099', 'DT编程科技有限公司', '行政部', 3, '2021-06-13 14:35:38', '2021-06-17 21:26:17');
INSERT INTO `sys_department` VALUES ('1000001620535597', '1000001776185099', 'DT编程科技有限公司', '客服部', 4, '2021-06-13 14:35:40', '2021-06-17 21:29:39');
INSERT INTO `sys_department` VALUES ('1000001625392933', '1000001341234088', '行政部', '法律部门', 1, '2021-06-17 21:33:05', '2021-06-17 21:33:05');
INSERT INTO `sys_department` VALUES ('1000001637526739', '1000001620535597', '客服部', '服务部门', 2, '2021-06-17 21:33:55', '2021-06-17 21:33:55');
INSERT INTO `sys_department` VALUES ('1000001728835022', '1000001637526739', '服务部门', '服务一部', 1, '2021-06-20 21:56:19', '2021-06-20 21:56:19');
INSERT INTO `sys_department` VALUES ('1000001776185099', '0', '顶级部门', 'DT编程科技有限公司', 1, '2021-06-13 14:35:42', '2021-11-14 11:22:51');
INSERT INTO `sys_department` VALUES ('1000001779686042', '1000001200689941', '技术部', '后端部门', 1, '2021-06-17 21:27:48', '2021-06-17 21:27:48');
INSERT INTO `sys_department` VALUES ('1000001854756787', '1000000622591924', '设计部', '设计组一', 1, '2021-06-20 21:54:18', '2021-06-20 21:54:18');
INSERT INTO `sys_department` VALUES ('1000001934748021', '1000000622591924', '设计部', '设计组二', 2, '2021-06-20 21:54:27', '2021-06-20 21:54:27');
INSERT INTO `sys_department` VALUES ('1000001975876013', '1000000622591924', '设计部', '设计组三', 3, '2021-06-20 21:54:48', '2021-06-20 21:54:48');
INSERT INTO `sys_department` VALUES ('1000002112552988', '1000000354532983', 'DT设计科技有限公司', '研发部', 1, '2021-06-20 21:54:57', '2021-06-20 21:54:57');

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log`  (
  `id` varchar(22) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志标题',
  `login_user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录账号',
  `login_ip` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '登录IP',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录浏览器',
  `operating_system` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作系统',
  `status` int NULL DEFAULT NULL COMMENT '登录状态：1成功 2失败',
  `type` int NULL DEFAULT NULL COMMENT '类型：1登录系统 2退出系统',
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作消息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------
INSERT INTO `sys_login_log` VALUES ('1000000378088885', '登录系统', 'admin', '127.0.0.1', 'Chrome', 'Windows 10 or Windows Server 2016', 1, 1, '登录成功', '2021-11-20 23:04:17', '2021-11-20 23:04:17');
INSERT INTO `sys_login_log` VALUES ('1000000647413177', '登录系统', 'admin', '127.0.0.1', 'Chrome', 'Windows 10 or Windows Server 2016', 1, 1, '登录成功', '2021-11-20 22:58:56', '2021-11-20 22:58:56');
INSERT INTO `sys_login_log` VALUES ('1000000698567554', '登录系统', 'admin', '127.0.0.1', 'Chrome', 'Windows 10 or Windows Server 2016', 1, 1, '登录成功', '2021-11-20 23:05:04', '2021-11-20 23:05:04');
INSERT INTO `sys_login_log` VALUES ('1000000789815459', '账号被锁', 'root', '127.0.0.1', 'Chrome', 'Windows 10 or Windows Server 2016', 2, 2, '登录失败，账号被锁定15分钟', '2021-11-20 23:01:17', '2021-11-20 23:01:17');
INSERT INTO `sys_login_log` VALUES ('1000000956147914', '账号被锁', 'test', '127.0.0.1', 'Chrome', 'Windows 10 or Windows Server 2016', 2, 1, '账号被锁定15分钟', '2021-11-20 23:03:50', '2021-11-20 23:03:50');
INSERT INTO `sys_login_log` VALUES ('1000002019704519', '登录系统', 'admin', '127.0.0.1', 'Chrome', 'Windows 10 or Windows Server 2016', 1, 1, '登录成功', '2021-11-20 22:58:26', '2021-11-20 22:58:26');
INSERT INTO `sys_login_log` VALUES ('1000002079599789', '登录系统', 'admin', '127.0.0.1', 'Chrome', 'Windows 10 or Windows Server 2016', 1, 1, '登录成功', '2021-11-20 23:01:25', '2021-11-20 23:01:25');

-- ----------------------------
-- Table structure for sys_operator_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_operator_log`;
CREATE TABLE `sys_operator_log`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `business_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作类型',
  `request_method_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求方法',
  `request_method_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求方法类型',
  `request_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作员',
  `request_url` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求URL',
  `request_ip` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求IP',
  `request_param` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求参数',
  `response_param` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '响应参数',
  `status` int NULL DEFAULT NULL COMMENT '操作状态：1正常 2异常',
  `error_info` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '错误信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_operator_log
-- ----------------------------
INSERT INTO `sys_operator_log` VALUES ('1000000247215363', '删除平台系统角色', 'delete', 'com.dt.app.controller.account.SysRoleController.delete()', 'DELETE', 'admin', '/api/v1/role/delete/40', '127.0.0.1', '40', '{\"code\":2000,\"msg\":\"操作成功!\"}', 1, NULL, '2021-11-20 20:51:10', '2021-11-20 20:51:10');
INSERT INTO `sys_operator_log` VALUES ('1000000321526495', '删除平台系统角色', 'delete', 'com.dt.app.controller.account.SysRoleController.delete()', 'DELETE', 'admin', '/api/v1/role/delete/41', '127.0.0.1', '41', '{\"code\":2000,\"msg\":\"操作成功!\"}', 1, NULL, '2021-11-20 20:51:12', '2021-11-20 20:51:12');
INSERT INTO `sys_operator_log` VALUES ('1000000420629317', '删除平台系统角色', 'delete', 'com.dt.app.controller.account.SysRoleController.delete()', 'DELETE', 'admin', '/api/v1/role/delete/38', '127.0.0.1', '38', '{\"code\":2000,\"msg\":\"操作成功!\"}', 1, NULL, '2021-11-20 20:48:30', '2021-11-20 20:48:30');
INSERT INTO `sys_operator_log` VALUES ('1000000597064637', '更新平台系统角色', 'update', 'com.dt.app.controller.account.SysRoleController.update()', 'PUT', 'admin', '/api/v1/role/update', '127.0.0.1', '{\"name\":\"FINANCE\",\"remark\":\"备注：财务部管理员描述\",\"id\":31}', '{\"code\":2000,\"msg\":\"操作成功!\"}', 1, NULL, '2021-11-20 20:48:49', '2021-11-20 20:48:49');
INSERT INTO `sys_operator_log` VALUES ('1000001358058078', '删除平台系统角色', 'delete', 'com.dt.app.controller.account.SysRoleController.delete()', 'DELETE', 'admin', '/api/v1/role/delete/37', '127.0.0.1', '37', '{\"code\":2000,\"msg\":\"操作成功!\"}', 1, NULL, '2021-11-20 20:48:27', '2021-11-20 20:48:27');
INSERT INTO `sys_operator_log` VALUES ('1000001404945205', '删除平台系统角色', 'delete', 'com.dt.app.controller.account.SysRoleController.delete()', 'DELETE', 'admin', '/api/v1/role/delete/39', '127.0.0.1', '39', '{\"code\":2000,\"msg\":\"操作成功!\"}', 1, NULL, '2021-11-20 20:48:33', '2021-11-20 20:48:33');
INSERT INTO `sys_operator_log` VALUES ('1000001874771042', '删除平台系统角色', 'delete', 'com.dt.app.controller.account.SysRoleController.delete()', 'DELETE', 'admin', '/api/v1/role/delete/43', '127.0.0.1', '43', '{\"code\":2000,\"msg\":\"操作成功!\"}', 1, NULL, '2021-11-20 20:45:36', '2021-11-20 20:45:36');
INSERT INTO `sys_operator_log` VALUES ('1000001962683932', '删除平台系统角色', 'delete', 'com.dt.app.controller.account.SysRoleController.delete()', 'DELETE', 'admin', '/api/v1/role/delete/42', '127.0.0.1', '42', '{\"code\":2000,\"msg\":\"操作成功!\"}', 1, NULL, '2021-11-20 20:51:14', '2021-11-20 20:51:14');
INSERT INTO `sys_operator_log` VALUES ('1000001966297287', '新增平台系统角色', 'insert', 'com.dt.app.controller.account.SysRoleController.save()', 'POST', 'admin', '/api/v1/role/save', '127.0.0.1', '{\"name\":\"aaaaaa\",\"remark\":\"bbbbb\"}', '{\"code\":2000,\"msg\":\"操作成功!\"}', 1, NULL, '2021-11-20 18:29:51', '2021-11-20 18:29:51');

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '权限 ID',
  `parent_id` bigint NULL DEFAULT NULL COMMENT '父权限 ID (0为顶级菜单)',
  `label` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '授权标识符',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '授权路径',
  `order_num` int NULL DEFAULT 0 COMMENT '序号',
  `type` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '类型(0 目录 1菜单，2按钮)',
  `icon` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '图标',
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `parent_name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '父级菜单名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 117 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (17, 0, '系统管理', 'sys:user:manage', '/system', NULL, NULL, 1, '0', 'el-icon-monitor', NULL, '2023-08-08 11:11:11', '2021-11-13 13:05:24', '顶级菜单');
INSERT INTO `sys_permission` VALUES (18, 17, '用户管理', 'sys:user', '/userList', 'userList', '/system/user/UserList', 3, '1', 'el-icon-user', NULL, '2023-08-08 11:11:11', '2021-10-31 12:36:43', '系统管理');
INSERT INTO `sys_permission` VALUES (20, 18, '新增', 'sys:user:add', NULL, NULL, '', 2, '2', '', '新增用户', '2023-08-08 11:11:11', '2021-06-20 20:07:50', '用户管理');
INSERT INTO `sys_permission` VALUES (21, 18, '修改', 'sys:user:update', NULL, NULL, '', 3, '2', '', '修改用户', '2023-08-08 11:11:11', '2021-06-20 20:08:17', '用户管理');
INSERT INTO `sys_permission` VALUES (22, 18, '删除', 'sys:user:del', NULL, NULL, '', 4, '2', '', '删除用户', '2023-08-08 11:11:11', '2021-06-20 20:08:08', '用户管理');
INSERT INTO `sys_permission` VALUES (23, 17, '角色管理', 'sys:role', '/roleList', 'roleList', '/system/role/RoleList', 4, '1', 'el-icon-lock', NULL, '2023-08-08 11:11:11', '2021-06-26 22:46:45', '系统管理');
INSERT INTO `sys_permission` VALUES (25, 23, '新增', 'sys:role:add', NULL, NULL, '', 2, '2', '', '新增角色', '2023-08-08 11:11:11', '2021-06-20 20:13:32', '角色管理');
INSERT INTO `sys_permission` VALUES (26, 23, '修改', 'sys:role:update', NULL, NULL, '', 3, '2', '', '修改角色', '2023-08-08 11:11:11', '2021-06-20 20:13:51', '角色管理');
INSERT INTO `sys_permission` VALUES (27, 23, '删除', 'sys:role:del', NULL, NULL, '', 4, '2', '', '删除角色', '2023-08-08 11:11:11', '2021-06-20 20:14:04', '角色管理');
INSERT INTO `sys_permission` VALUES (28, 17, '菜单管理', 'sys:menu', '/menuList', 'menuList', '/system/menu/MenuList', 5, '1', 'el-icon-s-operation', NULL, '2023-08-08 11:11:11', '2023-08-09 15:26:28', '系统管理');
INSERT INTO `sys_permission` VALUES (30, 28, '新增', 'sys:menu:add', NULL, NULL, '', 1, '2', NULL, '新增权限', '2023-08-08 11:11:11', '2021-06-20 21:04:12', '菜单管理');
INSERT INTO `sys_permission` VALUES (31, 28, '编辑', 'sys:menu:update', NULL, NULL, '', 2, '2', NULL, '修改权限', '2023-08-08 11:11:11', '2021-06-20 21:03:59', '菜单管理');
INSERT INTO `sys_permission` VALUES (32, 28, '删除', 'sys:menu:del', NULL, NULL, '', 3, '2', '', '删除权限', '2023-08-08 11:11:11', '2021-06-20 21:04:24', '菜单管理');
INSERT INTO `sys_permission` VALUES (33, 17, '机构管理', 'sys:dept', '/departmentList', 'departmentList', '/system/department/DepartmentList', 2, '1', 'el-icon-connection', '机构管理', '2020-04-12 22:58:29', '2021-06-27 00:23:02', '系统管理');
INSERT INTO `sys_permission` VALUES (42, 0, '参数配置', 'sys:config:manage', '/config', '', NULL, 5, '0', 'el-icon-setting', NULL, '2020-04-12 22:50:03', '2021-10-31 12:52:29', '顶级菜单');
INSERT INTO `sys_permission` VALUES (46, 33, '新增', 'sys:dept:add', '', '', NULL, 1, '2', '', NULL, '2020-04-12 19:58:48', '2021-06-20 21:02:05', '部门管理');
INSERT INTO `sys_permission` VALUES (76, 33, '编辑', 'sys:dept:update', '', '', NULL, 2, '2', '', NULL, '2020-04-12 20:42:20', '2021-10-06 15:08:21', '部门管理');
INSERT INTO `sys_permission` VALUES (77, 42, '参数配置', 'sys:config', '/configList', 'configList', '/config/ConfigList', 1, '1', 'el-icon-setting', NULL, '2020-04-13 11:31:45', '2021-10-27 21:16:23', '系统工具');
INSERT INTO `sys_permission` VALUES (78, 33, '删除', 'sys:dept:del', '', '', '', 3, '2', '', NULL, '2020-04-18 10:25:55', '2021-06-20 21:02:34', '机构管理');
INSERT INTO `sys_permission` VALUES (79, 23, '分配权限', 'sys:role:assign', '', '', '', 1, '2', '', NULL, '2020-04-18 10:31:05', '2021-06-20 20:13:38', '角色管理');
INSERT INTO `sys_permission` VALUES (80, 18, '分配角色', 'sys:user:assign', '', '', '', 1, '2', '', NULL, '2020-04-18 10:50:14', '2021-06-20 20:07:35', '用户管理');
INSERT INTO `sys_permission` VALUES (92, 0, '字典管理', 'sys:dic:manage', '/dic', '', '', 6, '0', 'el-icon-collection', NULL, '2021-06-27 00:36:23', '2021-07-07 21:19:59', '根节点');
INSERT INTO `sys_permission` VALUES (100, 0, '日志管理', 'sys:log:manage', '/log', '', '', 7, '0', 'el-icon-document-copy', NULL, '2021-07-07 21:26:02', '2021-11-20 19:43:23', '根节点');
INSERT INTO `sys_permission` VALUES (101, 100, '操作日志', 'sys:log:manage', '/operatorLogList', 'operatorLogList', '/log/OperatorLogList', 1, '1', 'el-icon-caret-right', NULL, '2021-07-07 21:26:59', '2021-11-20 19:50:07', '日志管理');
INSERT INTO `sys_permission` VALUES (103, 92, '一级菜单', 'a', 'a', 'a', 'a', 0, '1', 'el-icon-download', NULL, '2021-10-03 20:47:36', '2021-11-06 10:57:23', '字典管理');
INSERT INTO `sys_permission` VALUES (104, 103, '二级菜单', 'b', 'b', 'b', 'b', 0, '1', 'el-icon-video-camera', NULL, '2021-10-03 20:47:52', '2021-11-06 10:57:59', '一级菜单');
INSERT INTO `sys_permission` VALUES (105, 104, '区域列表', 'c', 'c', 'c', 'c', 0, '1', 'el-icon-s-home', NULL, '2021-10-03 20:48:06', '2021-11-06 11:01:14', '二级菜单');
INSERT INTO `sys_permission` VALUES (106, 105, '添加', 'd', '', '', '', 0, '2', '', NULL, '2021-10-03 20:48:22', '2021-11-06 11:01:22', '三级菜单');
INSERT INTO `sys_permission` VALUES (108, 28, '展开/折叠', 'sys:menu:expand', '', '', '', 4, '2', '', NULL, '2021-10-22 20:37:47', '2021-10-22 20:37:47', '菜单管理');
INSERT INTO `sys_permission` VALUES (109, 33, '展开/折叠', 'sys:dept:expand', '', '', '', 4, '2', '', NULL, '2021-10-23 15:45:18', '2021-10-23 15:45:18', '机构管理');
INSERT INTO `sys_permission` VALUES (110, 18, '导入', 'sys:user:import', '', '', '', 5, '2', '', NULL, '2021-10-23 19:45:38', '2021-10-23 19:45:46', '用户管理');
INSERT INTO `sys_permission` VALUES (111, 18, '导出', 'sys:user:export', '', '', '', 6, '2', '', NULL, '2021-10-23 19:46:18', '2021-10-23 19:46:18', '用户管理');
INSERT INTO `sys_permission` VALUES (114, 77, '添加', 'sys:config:add', '', '', '', 1, '2', '', NULL, '2021-11-06 11:48:24', '2021-11-06 11:48:24', '参数配置');
INSERT INTO `sys_permission` VALUES (115, 77, '编辑', 'sys:config:update', '', '', '', 2, '2', '', NULL, '2021-11-06 11:48:39', '2021-11-06 11:48:39', '参数配置');
INSERT INTO `sys_permission` VALUES (116, 77, '删除', 'sys:config:del', '', '', '', 0, '2', '', NULL, '2021-11-06 11:48:53', '2021-11-06 11:48:53', '参数配置');
INSERT INTO `sys_permission` VALUES (117, 100, '登录日志', 'sys:login:list', '/loginLogList', 'loginLogList', '/log/LoginLogList', 2, '1', 'el-icon-caret-right', NULL, '2021-11-20 19:50:58', '2021-11-20 19:51:10', '日志管理');
INSERT INTO `sys_permission` VALUES (118, 101, '删除', 'sys:operatorLog:del', '', '', '', 1, '2', '', NULL, '2021-11-20 20:17:20', '2021-11-20 20:17:20', '操作日志');
INSERT INTO `sys_permission` VALUES (119, 117, '删除', 'sys:loginLog:del', '', '', '', 1, '2', '', NULL, '2021-11-20 22:57:33', '2021-11-20 22:57:33', '登录日志');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色 ID',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色说明',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (9, 'ADMIN', '备注：系统超级管理员', '2020-05-20 07:51:28', '2021-09-03 21:05:57');
INSERT INTO `sys_role` VALUES (29, 'DEVELOP', '备注：开发部管理员', '2021-06-18 21:12:31', '2021-08-29 15:49:54');
INSERT INTO `sys_role` VALUES (30, 'MARKET', '备注：市场部管理员', '2021-06-18 21:13:11', '2021-08-29 15:49:50');
INSERT INTO `sys_role` VALUES (31, 'FINANCE', '备注：财务部管理员描述', '2021-06-18 21:13:55', '2021-11-20 20:48:49');
INSERT INTO `sys_role` VALUES (32, 'ROOT', '备注：普通角色', '2021-06-20 19:07:54', '2021-06-20 19:07:54');
INSERT INTO `sys_role` VALUES (33, 'TEST', '备注：测试角色', '2021-06-20 19:09:59', '2021-06-26 21:24:37');

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键 ID',
  `role_id` bigint NOT NULL COMMENT '角色 ID',
  `permission_id` bigint NOT NULL COMMENT '权限 ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1450 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (1443, 33, 92);
INSERT INTO `sys_role_permission` VALUES (1444, 33, 103);
INSERT INTO `sys_role_permission` VALUES (1445, 33, 104);
INSERT INTO `sys_role_permission` VALUES (1446, 33, 105);
INSERT INTO `sys_role_permission` VALUES (1447, 33, 106);
INSERT INTO `sys_role_permission` VALUES (1448, 33, 100);
INSERT INTO `sys_role_permission` VALUES (1449, 33, 101);
INSERT INTO `sys_role_permission` VALUES (1523, 9, 17);
INSERT INTO `sys_role_permission` VALUES (1524, 9, 33);
INSERT INTO `sys_role_permission` VALUES (1525, 9, 46);
INSERT INTO `sys_role_permission` VALUES (1526, 9, 76);
INSERT INTO `sys_role_permission` VALUES (1527, 9, 78);
INSERT INTO `sys_role_permission` VALUES (1528, 9, 109);
INSERT INTO `sys_role_permission` VALUES (1529, 9, 18);
INSERT INTO `sys_role_permission` VALUES (1530, 9, 80);
INSERT INTO `sys_role_permission` VALUES (1531, 9, 20);
INSERT INTO `sys_role_permission` VALUES (1532, 9, 21);
INSERT INTO `sys_role_permission` VALUES (1533, 9, 22);
INSERT INTO `sys_role_permission` VALUES (1534, 9, 110);
INSERT INTO `sys_role_permission` VALUES (1535, 9, 111);
INSERT INTO `sys_role_permission` VALUES (1536, 9, 23);
INSERT INTO `sys_role_permission` VALUES (1537, 9, 79);
INSERT INTO `sys_role_permission` VALUES (1538, 9, 25);
INSERT INTO `sys_role_permission` VALUES (1539, 9, 26);
INSERT INTO `sys_role_permission` VALUES (1540, 9, 27);
INSERT INTO `sys_role_permission` VALUES (1541, 9, 28);
INSERT INTO `sys_role_permission` VALUES (1542, 9, 30);
INSERT INTO `sys_role_permission` VALUES (1543, 9, 31);
INSERT INTO `sys_role_permission` VALUES (1544, 9, 32);
INSERT INTO `sys_role_permission` VALUES (1545, 9, 108);
INSERT INTO `sys_role_permission` VALUES (1546, 9, 42);
INSERT INTO `sys_role_permission` VALUES (1547, 9, 77);
INSERT INTO `sys_role_permission` VALUES (1548, 9, 116);
INSERT INTO `sys_role_permission` VALUES (1549, 9, 114);
INSERT INTO `sys_role_permission` VALUES (1550, 9, 115);
INSERT INTO `sys_role_permission` VALUES (1551, 9, 92);
INSERT INTO `sys_role_permission` VALUES (1552, 9, 103);
INSERT INTO `sys_role_permission` VALUES (1553, 9, 104);
INSERT INTO `sys_role_permission` VALUES (1554, 9, 105);
INSERT INTO `sys_role_permission` VALUES (1555, 9, 106);
INSERT INTO `sys_role_permission` VALUES (1556, 9, 100);
INSERT INTO `sys_role_permission` VALUES (1557, 9, 101);
INSERT INTO `sys_role_permission` VALUES (1558, 9, 118);
INSERT INTO `sys_role_permission` VALUES (1559, 9, 117);
INSERT INTO `sys_role_permission` VALUES (1560, 9, 119);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户 ID',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统登录名',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码，加密存储, admin/1234',
  `is_account_non_expired` int NULL DEFAULT 1 COMMENT '帐户是否过期(1 未过期，0已过期)',
  `is_account_non_locked` int NULL DEFAULT 1 COMMENT '帐户是否被锁定(1 未过期，0已过期)',
  `is_credentials_non_expired` int NULL DEFAULT 1 COMMENT '密码是否过期(1 未过期，0已过期)',
  `is_enabled` int NULL DEFAULT 1 COMMENT '帐户是否可用(1 可用，0 删除用户)',
  `dept_id` varchar(56) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门id',
  `dept_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册邮箱',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `account_name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `is_admin` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '1:管理员',
  `avatar` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '头像',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 72 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (9, 'admin', '$2a$10$MffA5vaTQMbrE8DQKqX8SusLa2CSogpEQ54Pp8o8ZDhUtBngs6R7O', 1, 1, 1, 1, '1000001779686042', '后端部门', '1973984292@qq.com', '2023-08-08 11:11:11', '2021-11-19 18:58:37', 'DT辰白', '1', 'http://47.108.191.196:9000/cms-bucket/20211119/6b52d75097164a04a0c7b319bb95e8a2.jpg');
INSERT INTO `sys_user` VALUES (52, 'hanxin', '$2a$10$OCJAq8QuSJ2mFZ/HmIv8V.eVucjFQOOOjijBz3agjbKBRePNdRYN2', 1, 1, 1, 1, '1000001779686042', '后端部门', 'hanxin@qq.com', '2021-06-19 08:54:34', '2021-10-01 23:36:21', '韩信', '0', NULL);
INSERT INTO `sys_user` VALUES (53, 'libai', '$2a$10$8E6gRGLIWrTSR1Bj9OTzuOv3MEaMmQaRnTXHc5cDbw61a5kC.fKiq', 1, 1, 1, 1, '1000000952846438', '财务部门', 'libai@qq.com', '2021-06-19 08:56:05', '2021-06-22 21:59:23', '李白', '0', NULL);
INSERT INTO `sys_user` VALUES (54, 'lyj', '$2a$10$BZVp9wUrelFHacqqdZp6fOjXj/ONaJzrX.pHzXCtdDidjxgMYkegK', 1, 1, 1, 1, '1000000204663981', '前端部门', 'linyoujia@qq.cm', '2021-06-19 11:15:34', '2021-06-22 21:59:07', '林宥嘉', '0', NULL);
INSERT INTO `sys_user` VALUES (55, 'cyx', '$2a$10$N30pA2/IileRe1ZkPUvwCOOx3wZF/a2RkZW4zjM/p76OHqsr.qux2', 1, 1, 1, 1, '1000001625392933', '法律部门', 'chengyixun@qq.com', '2021-06-19 11:16:20', '2021-06-22 21:58:33', '陈奕迅', '0', NULL);
INSERT INTO `sys_user` VALUES (56, 'ldh', '$2a$10$i2eT4E41KBOrta3pV/qIFuCGT3mnl9vJKoC9QDy6F2CDIqALI8mWu', 1, 1, 1, 1, '1000001637526739', '服务部门', 'liudehua@163.com', '2021-06-19 11:16:53', '2021-06-22 21:58:06', '刘德华', '0', NULL);
INSERT INTO `sys_user` VALUES (57, 'hcy', '$2a$10$Y7L6PFJ/Osc3xZSQi0pPkOef1yJaZq/1l8RHW6wMUErdnKTfGbeQW', 1, 1, 1, 1, '1000001258096779', 'UI设计部门', 'huachengyu@qq.com', '2021-06-19 11:17:36', '2021-06-22 21:57:05', '华晨宇', '0', NULL);
INSERT INTO `sys_user` VALUES (58, 'ljj', '$2a$10$JTU.nkbyFr06T235E5NZtOnqK1vD5k4AF05nCEBAmArPols/xcQny', 1, 1, 1, 0, '1000001779686042', '后端部门', 'linjunjie@qq.com', '2021-06-19 11:18:14', '2021-07-04 19:39:12', '林俊杰', '0', NULL);
INSERT INTO `sys_user` VALUES (59, 'zxy', '$2a$10$P1m48AeJGnfAqv4n43dbTOy3FDXSB2TzaHTBqRuQ/PmpSZZhwj1j.', 1, 1, 1, 0, '1000001258096779', 'UI设计部门', 'zhangxueyou@qq.com', '2021-06-19 11:18:54', '2021-07-04 19:39:09', '张学友', '0', NULL);
INSERT INTO `sys_user` VALUES (60, 'zjl', '$2a$10$85v6LqOp.TDmbU4dJfRebuctnWt3LgzWzjFEM.kCWmKGhz5nfdIG.', 1, 1, 1, 1, '1000001637526739', '服务部门', 'zhoujielun@qq.com', '2021-06-20 20:09:30', '2021-10-23 18:34:38', '周杰伦', '0', NULL);
INSERT INTO `sys_user` VALUES (62, 'zy', '$2a$10$7KoclgJ9q1hA9m7fh8pwyuqbBPLl9Y4FX8RCDQteKvEiNQ1t5hNEC', 1, 1, 1, 0, '1000001218109551', 'JAVA开发部', 'www.@qq.com', '2021-07-11 18:02:25', '2021-10-23 18:34:44', '赵云', '0', NULL);
INSERT INTO `sys_user` VALUES (63, 'zhangsan', '$2a$10$h3h6z0bfscWklKPI3rTd/e9fWx1/MjZ5YNnDFUylwejsDtXNEPQXi', 1, 1, 1, 1, '1000001218109551', 'JAVA开发部', 'zhangsan@qq.com', '2021-08-15 21:21:35', '2021-10-23 17:31:54', '张三', '0', NULL);
INSERT INTO `sys_user` VALUES (64, 'lisi', '$2a$10$25H.gloQF9Ni3GTkEYdmge3Jm.IbFqbGTxmtuooEKU1RMLoBarqdm', 1, 1, 1, 1, '1000001218109551', 'JAVA开发部', 'lisi@qq.com', '2021-08-15 21:22:36', '2021-10-23 20:10:35', '李四', '0', NULL);
INSERT INTO `sys_user` VALUES (65, 'test', '$2a$10$1Cmb9s0INlxEqs0yQOH0RObyfGTisVrs5Vx9NFKlQL7ZTTXA7M2Pe', 1, 1, 1, 1, '1000000819493000', 'C语言开发部', '111@qq.com', '2021-10-04 00:21:38', '2021-10-23 17:32:51', 'test测试', '0', NULL);
INSERT INTO `sys_user` VALUES (66, 'test1', '$2a$10$XRI9lligZwu.pPAGZlwUEOGAjLvL3EFh7WwRifNCAiRhqEd.OT8j.', 1, 1, 1, 1, '1000000204663981', '前端部门', 'ww@qq.com', '2021-10-23 17:33:57', '2021-10-23 17:39:26', 'test1', '0', NULL);
INSERT INTO `sys_user` VALUES (68, '123456', '$2a$10$QxFqhuaL/C9PaJrdhAOYKukW4fPAgywS7Udh9bMlbVVdfLNyAx39i', 1, 1, 1, 1, '1000001779686042', '后端部门', 'aaa@qq.com', '2021-11-05 16:54:35', '2021-11-05 16:54:35', '123456', '0', NULL);
INSERT INTO `sys_user` VALUES (70, 'root', '$2a$10$qSghbl3GF4ppzdWSgZf.bedAekLSuRZUUSoa3AInihVH3kxDn6sPW', 1, 1, 1, 1, '1000001779686042', '后端部门', '1973984292@qq.com', '2021-11-06 12:19:04', '2021-11-19 17:50:12', 'CMS管理员', '0', 'http://47.108.191.196:9000/cms-bucket/20211106/22a5dc6dbb4c4d8bb691b1dfbeae0d49.jpg');
INSERT INTO `sys_user` VALUES (71, 'test001', '$2a$10$phGm7uFZCOvkPHCP7VphoeTNZbegE2DNRCQ.pejYfct6tnaqQEGoq', 1, 1, 1, 1, '1000001779686042', '后端部门', '1@qq.com', '2021-11-19 17:51:37', '2021-11-19 17:51:37', 'test001', '0', NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键 ID',
  `user_id` bigint NOT NULL COMMENT '用户 ID',
  `role_id` bigint NOT NULL COMMENT '角色 ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 9, 9);
INSERT INTO `sys_user_role` VALUES (18, 52, 33);
INSERT INTO `sys_user_role` VALUES (19, 53, 33);
INSERT INTO `sys_user_role` VALUES (20, 54, 33);
INSERT INTO `sys_user_role` VALUES (21, 55, 33);
INSERT INTO `sys_user_role` VALUES (22, 56, 33);
INSERT INTO `sys_user_role` VALUES (23, 57, 33);
INSERT INTO `sys_user_role` VALUES (24, 58, 33);
INSERT INTO `sys_user_role` VALUES (25, 59, 33);
INSERT INTO `sys_user_role` VALUES (26, 60, 33);
INSERT INTO `sys_user_role` VALUES (27, 61, 33);
INSERT INTO `sys_user_role` VALUES (28, 62, 33);
INSERT INTO `sys_user_role` VALUES (29, 63, 33);
INSERT INTO `sys_user_role` VALUES (30, 64, 33);
INSERT INTO `sys_user_role` VALUES (31, 65, 33);
INSERT INTO `sys_user_role` VALUES (32, 66, 33);
INSERT INTO `sys_user_role` VALUES (33, 67, 9);
INSERT INTO `sys_user_role` VALUES (34, 68, 9);
INSERT INTO `sys_user_role` VALUES (35, 70, 33);
INSERT INTO `sys_user_role` VALUES (36, 71, 33);

SET FOREIGN_KEY_CHECKS = 1;
