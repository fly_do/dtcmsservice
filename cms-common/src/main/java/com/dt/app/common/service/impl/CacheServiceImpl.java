package com.dt.app.common.service.impl;

import com.dt.app.common.constant.ConstantCode;
import com.dt.app.common.service.AuthenticationInfoService;
import com.dt.app.common.service.CacheService;
import com.dt.app.common.service.RedisService;
import com.dt.app.modules.sys.entities.SysUserEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
 * @author DT
 * @date 2021/9/3 20:44
 */
@Service
public class CacheServiceImpl implements CacheService {

    @Resource
    private AuthenticationInfoService authenticationInfoService;
    @Resource
    private RedisService redisService;

    @Override
    public void removeUserCache() {
        // 删除用户基础信息缓存
        SysUserEntity sysUserEntity = (SysUserEntity) authenticationInfoService.getAuthentication();
        Object userCache = redisService.get(ConstantCode.USER_KEY + sysUserEntity.getUsername());
        Object authorCache = redisService.get(ConstantCode.PERMISSION_KEY + sysUserEntity.getId());
        if(!ObjectUtils.isEmpty(userCache)) {
            redisService.del(ConstantCode.USER_KEY + sysUserEntity.getUsername());
        }
        if(!ObjectUtils.isEmpty(authorCache)) {
            redisService.del(ConstantCode.PERMISSION_KEY + sysUserEntity.getId());
        }
    }
}
