package com.dt.app.common.service.impl;

import com.dt.app.common.service.AuthenticationInfoService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * 获取Security登录用户信息
 * @author DT
 * @date 2021/7/10 15:37
 */
@Service
public class AuthenticationInfoServiceImpl implements AuthenticationInfoService {

    @Override
    public Object getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
