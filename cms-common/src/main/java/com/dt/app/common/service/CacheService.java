package com.dt.app.common.service;

/**
 * @author DT
 * @date 2021/9/3 20:43
 */
public interface CacheService {

    void removeUserCache();
}
