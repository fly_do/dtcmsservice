package com.dt.app.common.constant;

import java.util.*;

/**
 * 系统参数配置常量
 * @author DT
 * @date 2021/11/3 20:48
 */
public class ConfigConstantCode {

    public static List<Map<String, String>> CONFIG_INFORMATION_LIST = new ArrayList<>();

    static {
        Map<String, String> configInformationMap = new LinkedHashMap<>();
        configInformationMap.put("name", ConstantCode.BASE_CONFIG_THEME_TYPE);
        Map<String, String> configInformationMap2 = new LinkedHashMap<>();
        configInformationMap2.put("name", ConstantCode.BASE_CONFIG_INDEX_TYPE);
        Map<String, String> configInformationMap3 = new LinkedHashMap<>();
        configInformationMap3.put("name", ConstantCode.BASE_CONFIG_MSG_TYPE);
        Map<String, String> configInformationMap4 = new LinkedHashMap<>();
        configInformationMap4.put("name", ConstantCode.BASE_CONFIG_LOGIN_TYPE);
        CONFIG_INFORMATION_LIST.add(configInformationMap);
        CONFIG_INFORMATION_LIST.add(configInformationMap2);
        CONFIG_INFORMATION_LIST.add(configInformationMap3);
        CONFIG_INFORMATION_LIST.add(configInformationMap4);
    }

    public static ConfigConstantCode configConstantCode;

    public static ConfigConstantCode getInstance(){
        if(configConstantCode == null){
            configConstantCode = new ConfigConstantCode();
        }
        return configConstantCode;
    }

    public List<Map<String, String>> getConfigData() {
        return CONFIG_INFORMATION_LIST;
    }

    public static void main(String[] args) {
        List<Map<String, String>> configData = ConfigConstantCode.getInstance().getConfigData();
        System.out.println(configData);
    }
}
