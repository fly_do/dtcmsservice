package com.dt.app.common.constant;

/**
 * @author DT
 * @date 2021/11/20 18:19
 */
public class LogTypeCode {

    public static final String LOGIN = "login";

    public static final String INSERT = "insert";

    public static final String UPDATE = "update";

    public static final String DELETE = "delete";

    public static final String BATCH_DELETE = "batch_delete";

    public static final String OTHER = "other";
}
