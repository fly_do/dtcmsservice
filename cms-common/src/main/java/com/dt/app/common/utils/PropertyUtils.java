package com.dt.app.common.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @author DT
 * @date 2021/11/5 22:02
 */
@Component
@Lazy(false)
public class PropertyUtils implements ApplicationContextAware {

    private static Environment environment;

    public static String getProperty(String key) {
        return environment.getProperty(key);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        PropertyUtils.environment = applicationContext.getBean(Environment.class);
    }
}
