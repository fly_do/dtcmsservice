package com.dt.app.common.service;

/**
 * @author DT
 * @date 2021/7/10 15:35
 */
public interface AuthenticationInfoService {

    /**
     * 获取登录用户信息
     * @return 返回用户对象
     */
    Object getAuthentication();

}
