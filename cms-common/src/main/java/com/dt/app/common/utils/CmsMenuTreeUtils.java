package com.dt.app.common.utils;

import com.dt.app.modules.sys.entities.SysPermissionEntity;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 权限菜单树生成工具类
 * @author DT
 * @date 2021/6/5 13:29
 */
public class CmsMenuTreeUtils {

    public static List<SysPermissionEntity> makeTree(List<SysPermissionEntity> menuList, Long pid){
        // 1.查询所有的pid的子类
        List<SysPermissionEntity> children = menuList.stream().filter(x -> x.getParentId().equals(pid)).collect(Collectors.toList());
        // 2.查询所有的非pid的子类
        List<SysPermissionEntity> successor = menuList.stream().filter(x -> !x.getParentId().equals(pid)).collect(Collectors.toList());
        if(children.size() > 0){
            children.forEach(x -> {
                if(successor.size() > 0){
                    makeTree(successor,x.getId()).forEach(
                            y ->x.getChildren().add(y)
                    );
                }
            });
        }
        return children;
    }
}
