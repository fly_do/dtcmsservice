package com.dt.app.modules.sys.vo.request;

import com.dt.app.modules.base.entites.PageEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 系统-用户分页查询参数接收类
 * @author DT
 * @date 2021/6/3 21:23
 */
@Data
@NoArgsConstructor
@ApiModel(value = "用户分页查询参数")
@EqualsAndHashCode(callSuper = false)
public class SysUserPageRequest extends PageEntity implements Serializable {

    private static final long serialVersionUID = -8690869129751501305L;

    @ApiModelProperty(value = "用户名")
    private String accountName;

    @ApiModelProperty(value = "部门ID")
    private String deptId;

    @ApiModelProperty(value = "帐户是否可用(1 可用，0 删除用户)")
    private String isEnabled;
}
