package com.dt.app.modules.sys.vo.request;

import com.dt.app.modules.base.entites.PageEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author DT
 * @date 2021/11/20 19:29
 */
@Data
@NoArgsConstructor
@ApiModel(value = "操作日志分页查询参数")
@EqualsAndHashCode(callSuper = false)
public class SysOperatorLogPageRequest extends PageEntity implements Serializable {

    private static final long serialVersionUID = 9051031173984497115L;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "操作业务类型")
    private String businessType;

    @ApiModelProperty(value = "请求方式")
    private String requestMethodType;

    @ApiModelProperty(value = "操作账号")
    private String requestUserName;

    @ApiModelProperty(value = "操作状态(1 正常, 2 异常)")
    private Integer status;

}
