package com.dt.app.modules.sys.vo.response;

import com.dt.app.modules.base.entites.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 系统-用户分页查询返回类
 * @author DT
 * @date 2021/6/3 21:25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "用户信息查询返回")
public class SysUserResponse extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 2050394360389330191L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "账号")
    private String username;

    @ApiModelProperty(value = "用户名")
    private String accountName;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "帐户是否过期(1 未过期，0已过期)")
    private boolean isAccountNonExpired = true;

    @ApiModelProperty(value = "帐户是否被锁定(1 未锁定，0已锁定)")
    private boolean isAccountNonLocked = true;

    @ApiModelProperty(value = "密码是否过期(1 未过期，0已过期)")
    private boolean isCredentialsNonExpired = true;

    @ApiModelProperty(value = "帐户是否可用(1 可用，0 删除用户)")
    private boolean isEnabled = true;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "是否是管理员(1 是, 0 不是)")
    private String isAdmin;

    @ApiModelProperty(value = "所属角色ID")
    private Long roleId;

    @ApiModelProperty(value = "角色用户关联ID")
    private Long userRoleId;
}
