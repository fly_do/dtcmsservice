package com.dt.app.modules.sys.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 系统-角色权限添加参数接收类
 * @author DT
 * @date 2021/6/20 14:57
 */
@Data
@NoArgsConstructor
@ApiModel(value = "角色权限新增参数")
public class SysRoleMenuRequest implements Serializable {

    private static final long serialVersionUID = 5135594183553077232L;

    @ApiModelProperty(value = "角色ID")
    private Long roleId;

    @ApiModelProperty(value = "权限ID集合")
    private List<Long> ids = new ArrayList<>();
}
