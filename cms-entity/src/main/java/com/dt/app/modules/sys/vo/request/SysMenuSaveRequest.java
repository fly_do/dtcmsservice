package com.dt.app.modules.sys.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * 系统-菜单添加参数接收类
 * @author DT
 * @date 2021/6/13 14:00
 */
@Data
@NoArgsConstructor
@ApiModel(value = "菜单新增参数")
public class SysMenuSaveRequest implements Serializable {

    private static final long serialVersionUID = -8159234148638765697L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "父节点ID (0为顶级菜单)")
    private Long parentId;

    @ApiModelProperty(value = "父节点名称")
    private String parentName;

    @ApiModelProperty(value = "菜单名称")
    @NotEmpty(message = "菜单不能为空")
    private String label;

    @ApiModelProperty(value = "授权标识符（增加权限控制细粒度）")
    @NotEmpty(message = "授权标识不能为空")
    private String code;

    @ApiModelProperty(value = "路由地址（以/开头）")
    private String path;

    @ApiModelProperty(value = "路由名称（字符串）")
    private String name;

    @ApiModelProperty(value = "授权路径（vue组件路径）")
    private String url;

    @ApiModelProperty(value = "排序序号")
    private Integer orderNum;

    @ApiModelProperty(value = "类型(0 目录 1菜单，2按钮)")
    private String type;

    @ApiModelProperty(value = "菜单图标")
    private String icon;

}
