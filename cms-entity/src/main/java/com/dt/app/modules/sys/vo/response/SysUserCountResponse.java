package com.dt.app.modules.sys.vo.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 统计部门用户数量
 * @author DT
 * @date 2021/11/20 0:50
 */
@Data
@Builder
public class SysUserCountResponse implements Serializable {

    private static final long serialVersionUID = -4293332698475290991L;

    @ApiModelProperty(value = "key")
    private String key;

    @ApiModelProperty(value = "value")
    private Integer value;
}
