package com.dt.app.modules.sys.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * 系统-角色添加参数接收类
 * @author DT
 * @date 2021/6/10 21:03
 */
@Data
@ApiModel(value = "角色新增参数")
public class SysRoleSaveRequest implements Serializable {

    private static final long serialVersionUID = -5471373721704009965L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @NotEmpty(message = "名称不能为空")
    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "角色备注")
    private String remark;

}
