package com.dt.app.modules.sys.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * 系统-部门添加参数接收类
 * @author DT
 * @date 2021/6/13 11:56
 */
@Data
@ApiModel(value = "部门新增参数")
public class SysDeptSaveRequest implements Serializable {

    private static final long serialVersionUID = -3431772847299203737L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "上级部门id")
    private String pid;

    @NotEmpty(message = "名称不能为空")
    @ApiModelProperty(value = "部门名称")
    private String name;

    @ApiModelProperty(value = "上级部门名称")
    private String parentName;

    @ApiModelProperty(value = "序号")
    private Integer orderNum;
}
