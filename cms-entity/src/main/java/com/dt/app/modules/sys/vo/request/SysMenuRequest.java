package com.dt.app.modules.sys.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 系统-菜单查询参数接收类
 * @author DT
 * @date 2021/6/13 14:00
 */
@Data
@NoArgsConstructor
@ApiModel(value = "菜单查询参数")
public class SysMenuRequest implements Serializable {

    private static final long serialVersionUID = -8159234148638765697L;

    @ApiModelProperty(value = "菜单名称")
    private String label;

    @ApiModelProperty(value = "类型(0 目录 1菜单，2按钮)")
    private String type;

}
