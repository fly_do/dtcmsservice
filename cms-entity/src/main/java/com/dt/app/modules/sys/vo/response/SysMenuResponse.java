package com.dt.app.modules.sys.vo.response;

import com.dt.app.modules.sys.entities.SysPermissionEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 系统：用户登录成功菜单封装返回
 * @author DT
 * @date 2021/6/5 13:19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户登录成功菜单封装返回")
@EqualsAndHashCode(callSuper = false)
public class SysMenuResponse implements Serializable {

    private static final long serialVersionUID = -5468244798566566786L;

    @ApiModelProperty(value = "菜单集合")
    private List<SysPermissionEntity> menuList;

    @ApiModelProperty(value = "权限集合")
    private List<String> authList;

    @ApiModelProperty(value = "路由集合")
    private List<SysPermissionEntity> routerList;

    @ApiModelProperty(value = "Token令牌")
    private String token;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "用户名")
    private String accountName;
}
