package com.dt.app.modules.sys.vo.response;

import com.dt.app.modules.base.entites.BaseEntity;
import com.dt.app.modules.sys.entities.SysPermissionEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author DT
 * @date 2021/6/12 0:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "角色列表返回对象")
public class SysRoleResponse extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 5718301546156960589L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "角色备注")
    private String remark;

    @ApiModelProperty(value = "权限集合")
    private List<SysPermissionEntity> children;
}
