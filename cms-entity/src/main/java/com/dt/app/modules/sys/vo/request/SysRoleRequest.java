package com.dt.app.modules.sys.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统-角色分页查询参数接收类
 * @author DT
 * @date 2021/6/12 11:28
 */
@Data
@NoArgsConstructor
@ApiModel(value = "角色列表查询参数")
@EqualsAndHashCode(callSuper = false)
public class SysRoleRequest implements Serializable {

    private static final long serialVersionUID = 1527626406335512078L;

    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "开始时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date endTime;

}
