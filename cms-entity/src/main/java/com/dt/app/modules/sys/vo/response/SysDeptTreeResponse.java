package com.dt.app.modules.sys.vo.response;

import com.dt.app.modules.base.entites.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 系统：部门树封装返回
 * @author DT
 * @date 2021/6/13 14:01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "部门树查询返回")
public class SysDeptTreeResponse extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 3072131921906004458L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "上级部门id")
    private String pid;

    @ApiModelProperty(value = "部门名称")
    private String name;

    @ApiModelProperty(value = "上级部门名称")
    private String parentName;

    @ApiModelProperty(value = "序号")
    private Integer orderNum;

    @ApiModelProperty(value = "子节点集合")
    private List<SysDeptTreeResponse> children = new ArrayList<>();
}
