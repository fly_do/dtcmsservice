package com.dt.app.modules.sys.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author DT
 * @date 2021/11/5 16:29
 */
@Data
@ApiModel(value = "用户修改密码接受参数")
public class SysUserPwdRequest implements Serializable {

    private static final long serialVersionUID = -7259308080767523261L;

    @ApiModelProperty(value = "用户id")
    private Long id;

    @ApiModelProperty(value = "用户旧密码")
    @NotEmpty(message = "用户旧密码不能为空")
    private String oldPassword;

    @ApiModelProperty(value = "用户新密码")
    @NotEmpty(message = "用户新密码不能为空")
    private String newPassword;

}
