package com.dt.app.modules.sys.vo.response;

import com.dt.app.modules.base.entites.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 系统-菜单查询返回类
 * @author DT
 * @date 2021/6/19 14:56
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "菜单信息查询返回")
public class SysMenuSaveResponse extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 3347040109533218328L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "父节点ID (0为顶级菜单)")
    private Long parentId;

    @ApiModelProperty(value = "父节点名称")
    private String parentName;

    @ApiModelProperty(value = "权限名称")
    private String label;

    @ApiModelProperty(value = "授权标识符（增加权限控制细粒度）")
    private String code;

    @ApiModelProperty(value = "路由地址（以/开头）")
    private String path;

    @ApiModelProperty(value = "路由名称（字符串）")
    private String name;

    @ApiModelProperty(value = "授权路径（vue组件路径）")
    private String url;

    @ApiModelProperty(value = "排序序号")
    private Integer orderNum;

    @ApiModelProperty(value = "类型(0 目录 1菜单，2按钮)")
    private String type;

    @ApiModelProperty(value = "图标")
    private String icon;

}
