package com.dt.app.modules.sys.vo.request;

import com.dt.app.modules.base.entites.PageEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author DT
 * @date 2021/11/20 21:27
 */
@Data
@NoArgsConstructor
@ApiModel(value = "登录日志分页查询参数")
@EqualsAndHashCode(callSuper = false)
public class SysLoginLogPageRequest extends PageEntity implements Serializable {

    private static final long serialVersionUID = -1804247077180837465L;

    @ApiModelProperty(value = "日志标题")
    private String title;

    @ApiModelProperty(value = "登录账号")
    private String loginUserName;

    @ApiModelProperty(value = "登录IP")
    private String loginIp;

    @ApiModelProperty(value = "登录浏览器")
    private String browser;

    @ApiModelProperty(value = "操作系统")
    private String operatingSystem;

    @ApiModelProperty(value = "登录状态(1 成功, 2 失败)")
    private Integer status;

    @ApiModelProperty(value = "登录类型(1 登录系统, 2 退出系统)")
    private Integer type;
}
