package com.dt.app.modules.config.entities;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dt.app.modules.base.entites.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 系统：全局参数配置实体
 * @author DT
 * @date 2021/10/28 19:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("base_config")
@ApiModel(value="系统参数配置实体")
@EqualsAndHashCode(callSuper = true)
public class BaseConfigEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 4867832735769648472L;

    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;

    @ApiModelProperty(value = "参数类型")
    private String category;

    @ApiModelProperty(value = "参数名称")
    private String name;

    @ApiModelProperty(value = "参数key")
    private String k;

    @ApiModelProperty(value = "参数value")
    private String v;

    @ApiModelProperty(value = "是否启用：1->是，0->否")
    private Boolean enabled;
}
