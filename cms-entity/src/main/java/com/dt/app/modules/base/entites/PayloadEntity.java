package com.dt.app.modules.base.entites;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Jwt令牌身份信息实体
 * @author DT
 * @date 2021/6/27 17:34
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Builder
public class PayloadEntity {

    @ApiModelProperty("主题")
    private String subject;

    @ApiModelProperty("签发时间")
    private Long issueTime;

    @ApiModelProperty("过期时间")
    private Long expTime;

    @ApiModelProperty("JWT的ID")
    private String jwtId;

    @ApiModelProperty("用户账号")
    private String username;

}
