package com.dt.app.modules.sys.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * 系统-用户添加参数接收类
 * @author DT
 * @date 2021/6/10 21:03
 */
@Data
@ApiModel(value = "用户新增参数")
public class SysUserSaveRequest implements Serializable {

    private static final long serialVersionUID = -5471373721704009965L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "账号")
    @NotEmpty(message = "账号不能为空")
    private String username;

    @ApiModelProperty(value = "用户名")
    @NotEmpty(message = "用户名称不能为空")
    private String accountName;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "是否是管理员(1 是, 0 不是)")
    private String isAdmin;

    @ApiModelProperty(value = "所属角色ID")
    private Long roleId;

    @ApiModelProperty(value = "角色用户关联ID")
    private Long userRoleId;

    @ApiModelProperty(value = "帐户是否可用(1 可用，0 删除用户)")
    private boolean isEnabled;

}
