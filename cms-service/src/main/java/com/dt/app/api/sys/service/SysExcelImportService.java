package com.dt.app.api.sys.service;

import java.util.List;

/**
 * @author DT
 * @date 2021/10/24 17:36
 */
public interface SysExcelImportService {

    void save(List<Object> list);
}
