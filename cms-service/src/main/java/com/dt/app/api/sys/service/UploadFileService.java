package com.dt.app.api.sys.service;

import com.dt.app.common.response.ResultUtil;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 文件服务接口
 * @author DT
 * @date 2021/6/2 22:19
 */
public interface UploadFileService {

    ResultUtil<?> bucketIsExist(String bucketName);

    ResultUtil<?> createBucket(String bucketName);

    ResultUtil<?> removeBucket(String bucketName);

    ResultUtil<?> uploadFile(MultipartFile file, String bucketName);

    void downloadFile(String bucketName, String fileName, HttpServletResponse response);

    ResultUtil<?> delFile(String bucketName, String fileName);

    ResultUtil<?> createBucketPolicy(String bucketName);

    ResultUtil<Map<String, Object>> uploadCover(MultipartFile file);
}
