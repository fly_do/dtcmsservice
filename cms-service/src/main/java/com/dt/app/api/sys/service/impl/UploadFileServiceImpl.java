package com.dt.app.api.sys.service.impl;

import com.dt.app.api.sys.service.UploadFileService;
import com.dt.app.common.constant.ConstantCode;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.common.uploadfile.minio.MinIoUploadFile;
import com.dt.app.common.utils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件服务服务实现类
 * @author DT
 * @date 2021/11/5 18:16
 */
@Service
public class UploadFileServiceImpl implements UploadFileService {
    
    @Autowired
    private MinIoUploadFile minIoUploadFile;

    @Override
    public ResultUtil<?> bucketIsExist(String bucketName) {
        Boolean isExist = minIoUploadFile.bucketIsExist(bucketName);
        return isExist ? ResultUtil.success(null) : ResultUtil.error("不存在");
    }

    @Override
    public ResultUtil<?> createBucket(String bucketName) {
        Boolean isExist = minIoUploadFile.bucketIsExist(bucketName);
        if(isExist){
            return ResultUtil.error("已经存在bucket");
        }
        Boolean bucket = minIoUploadFile.createBucket(bucketName);
        return bucket ? ResultUtil.success(null) : ResultUtil.error("创建失败");
    }

    @Override
    public ResultUtil<?> removeBucket(String bucketName) {
        Boolean removeBucket = minIoUploadFile.removeBucket(bucketName);
        return removeBucket ? ResultUtil.success(null) : ResultUtil.error("删除失败");
    }

    @Override
    public ResultUtil<?> uploadFile(MultipartFile file, String bucketName) {
        String uploadFile = minIoUploadFile.uploadFile(file, bucketName);
        if(StringUtils.isNotBlank(uploadFile)){
            return ResultUtil.success(uploadFile);
        }
        return ResultUtil.error("上传失败");
    }

    @Override
    public void downloadFile(String bucketName, String fileName, HttpServletResponse response) {
        minIoUploadFile.downloadFile(bucketName,fileName,response);
    }

    @Override
    public ResultUtil<?> delFile(String bucketName, String fileName) {
        Boolean aBoolean = minIoUploadFile.delFile(bucketName, fileName);
        return aBoolean ? ResultUtil.success(null) : ResultUtil.error("删除失败");
    }

    @Override
    public ResultUtil<?> createBucketPolicy(String bucketName) {
        Boolean isExist = minIoUploadFile.bucketIsExist(bucketName);
        if(isExist){
            return ResultUtil.error("已经存在bucket");
        }
        Boolean bucket = minIoUploadFile.createBucketPolicy(bucketName);
        return bucket ? ResultUtil.success(null) : ResultUtil.error("创建失败");
    }

    @Override
    public ResultUtil<Map<String, Object>> uploadCover(MultipartFile file) {
        Boolean isExist = minIoUploadFile.bucketIsExist(ConstantCode.MINIO_COMMON_BUCKET_NAME);
        if(!isExist) {
            minIoUploadFile.createBucketPolicy(ConstantCode.MINIO_COMMON_BUCKET_NAME);
        }
        String uploadFile = minIoUploadFile.uploadFile(file, ConstantCode.MINIO_COMMON_BUCKET_NAME);
        Map<String, Object> map = new HashMap<>(2);
        if(StringUtils.isNotBlank(uploadFile)) {
            String uploadUrl = PropertyUtils.getProperty("minio.http.url");
            uploadUrl = uploadUrl.endsWith("/") ? uploadUrl : uploadUrl + "/";
            uploadUrl = uploadUrl + ConstantCode.MINIO_COMMON_BUCKET_NAME + "/" + uploadFile;
            map.put("url",uploadUrl);
            map.put("type",uploadFile.substring(uploadFile.lastIndexOf(".") + 1));
            return ResultUtil.success(map);
        }
        return ResultUtil.error("上传失败");
    }
}
