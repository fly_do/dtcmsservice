package com.dt.app.api.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.app.api.sys.mapper.SysUserRoleMapper;
import com.dt.app.api.sys.service.SysUserRoleService;
import com.dt.app.modules.sys.entities.SysUserRoleEntity;
import org.springframework.stereotype.Service;

/**
 * 系统用户关联角色服务实现类
 * @author DT
 * @date 2021/6/2 22:23
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRoleEntity> implements SysUserRoleService {

}
