package com.dt.app.api.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dt.app.modules.sys.entities.SysPermissionEntity;
import com.dt.app.modules.sys.vo.request.SysMenuRequest;
import com.dt.app.modules.sys.vo.response.SysMenuTreeResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统权限数据接口
 * @author DT
 * @date 2021/6/2 22:24
 */
public interface SysPermissionMapper extends BaseMapper<SysPermissionEntity> {

    List<SysPermissionEntity> getPermissionListByUserId(@Param("userId") Long userId);

    List<SysMenuTreeResponse> queryList(@Param("request") SysMenuRequest request);

    List<SysPermissionEntity> getPermissionListByRoleId(@Param("roleId") Integer roleId);
}
