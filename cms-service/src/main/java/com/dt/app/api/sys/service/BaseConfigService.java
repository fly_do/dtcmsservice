package com.dt.app.api.sys.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.modules.config.entities.BaseConfigEntity;

import java.util.List;

/**
 * 系统参数配置接口
 * @author DT
 * @date 2021/10/29 23:04
 */
public interface BaseConfigService extends IService<BaseConfigEntity>  {

    void initConfigData();

    ResultUtil<JSONObject> getInitData();

    ResultUtil<List<BaseConfigEntity>> queryThemeList();

    ResultUtil<BaseConfigEntity> updateThemeEnabled(String id);

    ResultUtil<?> listCategory();

    ResultUtil<BaseConfigEntity> saveConfig(BaseConfigEntity request);

    ResultUtil<List<BaseConfigEntity>> queryList(BaseConfigEntity request);

    ResultUtil<BaseConfigEntity> updateConfig(BaseConfigEntity request);

    ResultUtil<BaseConfigEntity> deleteConfig(String id);
}
