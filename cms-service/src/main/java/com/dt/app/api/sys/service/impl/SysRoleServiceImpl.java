package com.dt.app.api.sys.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.app.api.sys.mapper.SysRoleMapper;
import com.dt.app.api.sys.service.SysPermissionService;
import com.dt.app.api.sys.service.SysRolePermissionService;
import com.dt.app.api.sys.service.SysRoleService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.common.service.CacheService;
import com.dt.app.modules.sys.entities.SysPermissionEntity;
import com.dt.app.modules.sys.entities.SysRoleEntity;
import com.dt.app.modules.sys.entities.SysRolePermissionEntity;
import com.dt.app.modules.sys.vo.request.SysRoleRequest;
import com.dt.app.modules.sys.vo.request.SysRoleSaveRequest;
import com.dt.app.modules.sys.vo.response.SysRoleResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 系统角色服务实现类
 * @author DT
 * @date 2021/6/2 22:23
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRoleEntity> implements SysRoleService {

    @Resource
    private CacheService cacheService;
    @Resource
    private SysPermissionService sysPermissionService;
    @Resource
    private SysRolePermissionService sysRolePermissionService;

    @Override
    @Transactional(readOnly = true)
    public ResultUtil<List<SysRoleResponse>> queryList(SysRoleRequest request) {
        List<SysRoleResponse> responseList = new ArrayList<>();
        QueryWrapper<SysRoleEntity> wrapper = new QueryWrapper<>();
        if(StringUtils.isNotBlank(request.getName())) {
            wrapper.like("name",request.getName());
        }
        if(null != request.getStartTime() && null != request.getEndTime()) {
            wrapper.between("create_time"
                    ,DateFormatUtils.format(request.getStartTime(),"yyyy-MM-dd HH:mm:ss")
                    ,DateFormatUtils.format(request.getEndTime(),"yyyy-MM-dd HH:mm:ss"));
        }
        List<SysRoleEntity> roleEntityList = this.baseMapper.selectList(wrapper);
        if(!roleEntityList.isEmpty()) {
            roleEntityList.forEach(it -> {
                SysRoleResponse roleResponse = new SysRoleResponse();
                roleResponse.setId(it.getId());
                roleResponse.setName(it.getName());
                roleResponse.setRemark(it.getRemark());
                roleResponse.setCreateTime(it.getCreateTime());
                roleResponse.setUpdateTime(it.getUpdateTime());
                // 根据角色ID查询角色权限信息
                List<SysPermissionEntity> permissionEntityList = sysPermissionService.getPermissionListByRoleId(Math.toIntExact(it.getId()));
                List<SysPermissionEntity> childrenList = buildTree(permissionEntityList, 0L);
                roleResponse.setChildren(childrenList);
                responseList.add(roleResponse);
            });
        }
        return ResultUtil.success(responseList);
    }

    public static List<SysPermissionEntity> buildTree(List<SysPermissionEntity> list, Long pid){
        List<SysPermissionEntity> children = list.stream().filter(x -> x.getChildren() != null && x.getParentId().equals(pid)).collect(Collectors.toList());
        List<SysPermissionEntity> subclass = list.stream().filter(x -> x.getChildren() != null && !x.getParentId().equals(pid)).collect(Collectors.toList());
        if(children.size() > 0) {
            children.forEach(x -> {
                if(subclass.size() > 0) {
                    buildTree(subclass,x.getId()).forEach(y -> x.getChildren().add(y));
                }
            });
        }
        return children;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<SysRoleSaveRequest> saveRole(SysRoleSaveRequest request) {
        this.cacheService.removeUserCache();
        SysRoleEntity entity = new SysRoleEntity();
        BeanUtil.copyProperties(request,entity);
        int insert = this.baseMapper.insert(entity);
        return insert > 0 ? ResultUtil.success() : ResultUtil.error();
    }

    @Override
    @Transactional(readOnly = true)
    public ResultUtil<SysRoleEntity> getRoleById(Integer id) {
        SysRoleEntity entity = this.baseMapper.selectById(id);
        return ResultUtil.success(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<SysRoleSaveRequest> updateRole(SysRoleSaveRequest request) {
        this.cacheService.removeUserCache();
        SysRoleEntity entity = new SysRoleEntity();
        BeanUtil.copyProperties(request,entity);
        int update = this.baseMapper.updateById(entity);
        return update > 0 ? ResultUtil.success() : ResultUtil.error();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<SysRoleEntity> delete(Integer id) {
        this.cacheService.removeUserCache();
        // 删除角色权限中间表
        List<SysRolePermissionEntity> entities = this.sysRolePermissionService.list(new QueryWrapper<SysRolePermissionEntity>().eq("role_id", id));
        if(!entities.isEmpty()) {
            entities.forEach(it -> this.sysRolePermissionService.removeById(it.getId()));
        }
        int delete = this.baseMapper.deleteById(id);
        return delete > 0 ? ResultUtil.success() : ResultUtil.error();
    }

    @Override
    @Transactional(readOnly = true)
    public ResultUtil<List<SysRoleEntity>> findAll() {
        List<SysRoleEntity> entityList = this.baseMapper.selectList(null);
        return ResultUtil.success(entityList);
    }
}
