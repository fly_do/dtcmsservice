package com.dt.app.api.sys.service.impl;

import com.dt.app.api.sys.service.SysExcelImportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author DT
 * @date 2021/10/24 17:47
 */
@Service
public class SysExcelImportServiceImpl implements SysExcelImportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SysExcelImportServiceImpl.class);

    @Override
    public void save(List<Object> list) {
        LOGGER.info("调用service接口->>>保存数据:{}", list);
    }
}
