package com.dt.app.api.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dt.app.modules.sys.entities.SysLoginLogEntity;
import com.dt.app.modules.sys.vo.request.SysLoginLogPageRequest;

/**
 * 系统登录日志数据接口
 * @author DT
 * @date 2021/11/20 21:29
 */
public interface SysLoginLogMapper extends BaseMapper<SysLoginLogEntity> {

    IPage<SysLoginLogEntity> queryList(Page<SysLoginLogEntity> page, SysLoginLogPageRequest request);
}
