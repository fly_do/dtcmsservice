package com.dt.app.api.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dt.app.modules.sys.entities.SysRolePermissionEntity;

import java.util.List;

/**
 * 系统角色关联权限服务接口
 * @author DT
 * @date 2021/6/2 22:19
 */
public interface SysRolePermissionService extends IService<SysRolePermissionEntity> {

    void saveRoleMenu(Long roleId, List<Long> ids);
}
