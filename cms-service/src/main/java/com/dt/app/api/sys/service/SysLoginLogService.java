package com.dt.app.api.sys.service;

import cn.hutool.http.useragent.UserAgent;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.modules.sys.entities.SysLoginLogEntity;
import com.dt.app.modules.sys.vo.request.SysLoginLogPageRequest;

/**
 * @author DT
 * @date 2021/11/20 21:28
 */
public interface SysLoginLogService extends IService<SysLoginLogEntity> {

    void saveLoginLog(String key, String loginUserName, UserAgent userAgent, String ip);

    ResultUtil<IPage<SysLoginLogEntity>> queryLoginLogList(SysLoginLogPageRequest request);

    ResultUtil<?> deleteLoginLogById(String id);
}
