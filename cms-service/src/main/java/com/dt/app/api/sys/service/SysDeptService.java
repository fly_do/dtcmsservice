package com.dt.app.api.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.modules.sys.entities.SysDepartmentEntity;
import com.dt.app.modules.sys.vo.request.SysDeptRequest;
import com.dt.app.modules.sys.vo.request.SysDeptSaveRequest;
import com.dt.app.modules.sys.vo.response.SysDeptTreeResponse;

import java.util.List;

/**
 * 系统部门服务接口
 * @author DT
 * @date 2021/6/13 12:03
 */
public interface SysDeptService extends IService<SysDepartmentEntity> {

    ResultUtil<List<SysDeptTreeResponse>> queryList(SysDeptRequest request);

    ResultUtil<SysDeptSaveRequest> saveDept(SysDeptSaveRequest request);

    ResultUtil<SysDepartmentEntity> getDeptById(String id);

    ResultUtil<SysDeptSaveRequest> updateDept(SysDeptSaveRequest request);

    ResultUtil<SysDepartmentEntity> deleteDept(String id);

    ResultUtil<List<SysDeptTreeResponse>> listTree(Integer type);
}
