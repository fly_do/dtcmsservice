package com.dt.app.api.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dt.app.modules.config.entities.BaseConfigEntity;

/**
 * 系统参数配置接口
 * @author DT
 * @date 2021/10/29 23:06
 */
public interface BaseConfigMapper extends BaseMapper<BaseConfigEntity> {
}
