package com.dt.app.api.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dt.app.modules.sys.entities.SysOperatorLogEntity;
import com.dt.app.modules.sys.vo.request.SysOperatorLogPageRequest;

/**
 * 系统操作日志数据接口
 * @author DT
 * @date 2021/11/20 17:37
 */
public interface SysOperatorLogMapper extends BaseMapper<SysOperatorLogEntity> {

    IPage<SysOperatorLogEntity> queryList(Page<SysOperatorLogEntity> page, SysOperatorLogPageRequest request);
}
