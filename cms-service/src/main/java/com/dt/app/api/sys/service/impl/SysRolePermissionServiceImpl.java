package com.dt.app.api.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.app.api.sys.mapper.SysRolePermissionMapper;
import com.dt.app.api.sys.service.SysRolePermissionService;
import com.dt.app.common.service.CacheService;
import com.dt.app.modules.sys.entities.SysRolePermissionEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统角色关联权限服务实现类
 * @author DT
 * @date 2021/6/2 22:23
 */
@Service
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermissionEntity> implements SysRolePermissionService {

    @Resource
    private CacheService cacheService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveRoleMenu(Long roleId, List<Long> ids) {
        // 1、删除缓存数据
        this.cacheService.removeUserCache();
        // 2.删除该角色原来的全部权限
        QueryWrapper<SysRolePermissionEntity> query = new QueryWrapper<>();
        query.lambda().eq(SysRolePermissionEntity::getRoleId,roleId);
        this.baseMapper.delete(query);
        // 3.保存新的权限
        this.baseMapper.saveRolePermissions(roleId,ids);
    }
}
