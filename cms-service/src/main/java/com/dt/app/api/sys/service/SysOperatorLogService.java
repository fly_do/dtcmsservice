package com.dt.app.api.sys.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.modules.sys.entities.SysOperatorLogEntity;
import com.dt.app.modules.sys.vo.request.SysOperatorLogPageRequest;

/**
 * @author DT
 * @date 2021/11/20 17:37
 */
public interface SysOperatorLogService extends IService<SysOperatorLogEntity> {

    void saveOperatorLog(SysOperatorLogEntity sysLog);

    ResultUtil<IPage<SysOperatorLogEntity>> queryOperatorLogList(SysOperatorLogPageRequest request);

    ResultUtil<?> deleteOperatorLogById(String id);
}
