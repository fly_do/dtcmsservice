package com.dt.app.api.sys.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.app.api.sys.mapper.SysPermissionMapper;
import com.dt.app.api.sys.service.SysPermissionService;
import com.dt.app.api.sys.service.SysRolePermissionService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.common.service.CacheService;
import com.dt.app.modules.sys.entities.SysPermissionEntity;
import com.dt.app.modules.sys.entities.SysRolePermissionEntity;
import com.dt.app.modules.sys.vo.request.SysMenuRequest;
import com.dt.app.modules.sys.vo.request.SysMenuSaveRequest;
import com.dt.app.modules.sys.vo.response.SysMenuTreeResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 系统权限服务实现类
 * @author DT
 * @date 2021/6/2 22:23
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermissionEntity> implements SysPermissionService {

    @Resource
    private SysRolePermissionService sysRolePermissionService;
    @Resource
    private CacheService cacheService;

    @Override
    @Transactional(readOnly = true)
    public List<SysPermissionEntity> getPermissionListByUserId(Long userId) {
        return this.baseMapper.getPermissionListByUserId(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public ResultUtil<List<SysMenuTreeResponse>> queryList(SysMenuRequest request) {
        List<SysMenuTreeResponse> entityList = this.baseMapper.queryList(request);
        // 查询条件
        if(StringUtils.isNotBlank(request.getLabel()) || StringUtils.isNotBlank(request.getType())) {
            return ResultUtil.success(entityList);
        }
        // 构建树结构数据返回
        List<SysMenuTreeResponse> treeEntityList = new ArrayList<>();
        if(null != entityList && !entityList.isEmpty()){
            treeEntityList =  buildTree(entityList, 0L);
        }
        return ResultUtil.success(treeEntityList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<SysMenuSaveRequest> saveMenu(SysMenuSaveRequest request) {
        this.cacheService.removeUserCache();
        SysPermissionEntity entity = new SysPermissionEntity();
        BeanUtil.copyProperties(request,entity);
        int insert = this.baseMapper.insert(entity);
        return insert > 0 ? ResultUtil.success() : ResultUtil.error();
    }

    @Override
    @Transactional(readOnly = true)
    public ResultUtil<SysPermissionEntity> getMenuById(Long id) {
        SysPermissionEntity entity = this.baseMapper.selectById(id);
        return ResultUtil.success(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<SysMenuSaveRequest> updateMenu(SysMenuSaveRequest request) {
        this.cacheService.removeUserCache();
        SysPermissionEntity entity = new SysPermissionEntity();
        BeanUtil.copyProperties(request,entity);
        int update = this.baseMapper.updateById(entity);
        return update > 0 ? ResultUtil.success() : ResultUtil.error();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<SysPermissionEntity> deleteMenu(Long id) {
        this.cacheService.removeUserCache();
        Integer selectCount = this.baseMapper.selectCount(new QueryWrapper<SysPermissionEntity>().eq("parent_id", id));
        if(null != selectCount && selectCount > 0){
            return ResultUtil.error("请先删除子节点！");
        }
        int count = sysRolePermissionService.count(new QueryWrapper<SysRolePermissionEntity>().eq("permission_id", id));
        if(count > 0){
            return ResultUtil.error("该节点已有角色使用！");
        }
        int delete = this.baseMapper.deleteById(id);
        return delete > 0 ? ResultUtil.success() : ResultUtil.error();
    }

    @Override
    @Transactional(readOnly = true)
    public List<SysPermissionEntity> getPermissionListByRoleId(Integer roleId) {
        return this.baseMapper.getPermissionListByRoleId(roleId);
    }

    @Override
    @Transactional(readOnly = true)
    public ResultUtil<List<SysMenuTreeResponse>> listTree() {
        List<SysMenuTreeResponse> entityList = new ArrayList<>();
        List<SysPermissionEntity> sysPermissionEntityList = this.baseMapper.selectList(null);
        if(!sysPermissionEntityList.isEmpty()) {
            sysPermissionEntityList.forEach(it -> {
                SysMenuTreeResponse response = new SysMenuTreeResponse();
                BeanUtil.copyProperties(it,response);
                entityList.add(response);
            });
        }
        // 构建树结构数据返回
        List<SysMenuTreeResponse> treeEntityList = new ArrayList<>();
        if(!entityList.isEmpty()) {
            treeEntityList =  buildTree(entityList, 0L);
        }
        // 添加顶级节点
        SysMenuTreeResponse response = new SysMenuTreeResponse();
        response.setId(0L);
        response.setParentId(-1L);
        response.setOrderNum(0);
        response.setLabel("DTBoot管理系统");
        treeEntityList.add(response);
        // 排序输出
        return ResultUtil.success(treeEntityList.stream().sorted(Comparator.comparing(SysMenuTreeResponse::getOrderNum)).collect(Collectors.toList()));
    }

    public static List<SysMenuTreeResponse> buildTree(List<SysMenuTreeResponse> list, Long pid){
        List<SysMenuTreeResponse> children = list.stream().filter(x -> x.getChildren() != null && x.getParentId().equals(pid)).collect(Collectors.toList());
        List<SysMenuTreeResponse> subclass = list.stream().filter(x -> x.getChildren() != null && !x.getParentId().equals(pid)).collect(Collectors.toList());
        if(children.size() > 0) {
            children.forEach(x -> {
                if(subclass.size() > 0) {
                    buildTree(subclass,x.getId()).forEach(
                            y -> x.getChildren().add(y)
                    );
                }
            });
        }
        return children;
    }
}
