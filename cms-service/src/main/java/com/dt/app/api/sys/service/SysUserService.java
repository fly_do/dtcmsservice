package com.dt.app.api.sys.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.modules.sys.entities.SysUserEntity;
import com.dt.app.modules.sys.vo.request.SysUserPageRequest;
import com.dt.app.modules.sys.vo.request.SysUserPwdRequest;
import com.dt.app.modules.sys.vo.request.SysUserSaveRequest;
import com.dt.app.modules.sys.vo.response.SysUserPageResponse;
import com.dt.app.modules.sys.vo.response.SysUserResponse;

import java.util.Map;

/**
 * 系统用户服务接口
 * @author DT
 * @date 2021/6/2 22:19
 */
public interface SysUserService extends IService<SysUserEntity> {

    ResultUtil<IPage<SysUserPageResponse>> queryList(SysUserPageRequest request);

    ResultUtil<SysUserSaveRequest> saveUser(SysUserSaveRequest request);

    ResultUtil<SysUserResponse> getUserById(Long id);

    ResultUtil<SysUserSaveRequest> updateUser(SysUserSaveRequest request);

    ResultUtil<SysUserEntity> deleteUser(Long id);

    ResultUtil<?> updateUserStatus(Long id, Boolean enabled);

    ResultUtil<Boolean> updatePwd(SysUserPwdRequest request);

    ResultUtil<Boolean> updateAvatar(SysUserEntity request);

    ResultUtil<Map<String,Object>> queryUserCountByDept();
}
