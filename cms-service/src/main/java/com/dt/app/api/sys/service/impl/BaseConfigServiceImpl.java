package com.dt.app.api.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.app.api.sys.mapper.BaseConfigMapper;
import com.dt.app.api.sys.service.BaseConfigService;
import com.dt.app.common.constant.ConfigConstantCode;
import com.dt.app.common.constant.ConstantCode;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.common.utils.UuidUtils;
import com.dt.app.modules.config.entities.BaseConfigEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author DT
 * @date 2021/10/29 23:04
 */
@Service
public class BaseConfigServiceImpl extends ServiceImpl<BaseConfigMapper, BaseConfigEntity> implements BaseConfigService {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void initConfigData() {
        // 添加系统系统主题一
        Integer oneCount = this.baseMapper.selectCount(new QueryWrapper<BaseConfigEntity>()
                .eq("k", ConstantCode.BASE_CONFIG_THEME_K_ONE)
                .eq("v", ConstantCode.BASE_CONFIG_THEME_V_ONE));
        if(oneCount == null || oneCount == 0) {
            BaseConfigEntity entity = new BaseConfigEntity();
            entity.setId(UuidUtils.getUniqueIdByUUId());
            entity.setCategory(ConstantCode.BASE_CONFIG_THEME_TYPE);
            entity.setName("主题一");
            entity.setK(ConstantCode.BASE_CONFIG_THEME_K_ONE);
            entity.setV(ConstantCode.BASE_CONFIG_THEME_V_ONE);
            entity.setEnabled(true);
            this.baseMapper.insert(entity);
        }
        // 系统主题二
        Integer twoCount = this.baseMapper.selectCount(new QueryWrapper<BaseConfigEntity>()
                .eq("k", ConstantCode.BASE_CONFIG_THEME_K_TWO)
                .eq("v", ConstantCode.BASE_CONFIG_THEME_V_TWO));
        if(twoCount == null || twoCount == 0) {
            BaseConfigEntity entity = new BaseConfigEntity();
            entity.setId(UuidUtils.getUniqueIdByUUId());
            entity.setCategory(ConstantCode.BASE_CONFIG_THEME_TYPE);
            entity.setName("主题二");
            entity.setK(ConstantCode.BASE_CONFIG_THEME_K_TWO);
            entity.setV(ConstantCode.BASE_CONFIG_THEME_V_TWO);
            entity.setEnabled(false);
            this.baseMapper.insert(entity);
        }
        // 系统主题三
        Integer threeCount = this.baseMapper.selectCount(new QueryWrapper<BaseConfigEntity>()
                .eq("k", ConstantCode.BASE_CONFIG_THEME_K_THREE)
                .eq("v", ConstantCode.BASE_CONFIG_THEME_V_THREE));
        if(threeCount == null || threeCount == 0) {
            BaseConfigEntity entity = new BaseConfigEntity();
            entity.setId(UuidUtils.getUniqueIdByUUId());
            entity.setCategory(ConstantCode.BASE_CONFIG_THEME_TYPE);
            entity.setName("主题三");
            entity.setK(ConstantCode.BASE_CONFIG_THEME_K_THREE);
            entity.setV(ConstantCode.BASE_CONFIG_THEME_V_THREE);
            entity.setEnabled(false);
            this.baseMapper.insert(entity);
        }
        // 系统主题四
        Integer fourCount = this.baseMapper.selectCount(new QueryWrapper<BaseConfigEntity>()
                .eq("k", ConstantCode.BASE_CONFIG_THEME_K_FOUR)
                .eq("v", ConstantCode.BASE_CONFIG_THEME_V_FOUR));
        if(fourCount == null || fourCount == 0) {
            BaseConfigEntity entity = new BaseConfigEntity();
            entity.setId(UuidUtils.getUniqueIdByUUId());
            entity.setCategory(ConstantCode.BASE_CONFIG_THEME_TYPE);
            entity.setName("主题四");
            entity.setK(ConstantCode.BASE_CONFIG_THEME_K_FOUR);
            entity.setV(ConstantCode.BASE_CONFIG_THEME_V_FOUR);
            entity.setEnabled(false);
            this.baseMapper.insert(entity);
        }
        // 系统主题五
        Integer fiveCount = this.baseMapper.selectCount(new QueryWrapper<BaseConfigEntity>()
                .eq("k", ConstantCode.BASE_CONFIG_THEME_K_FIVE)
                .eq("v", ConstantCode.BASE_CONFIG_THEME_V_FIVE));
        if(fiveCount == null || fiveCount == 0) {
            BaseConfigEntity entity = new BaseConfigEntity();
            entity.setId(UuidUtils.getUniqueIdByUUId());
            entity.setCategory(ConstantCode.BASE_CONFIG_THEME_TYPE);
            entity.setName("主题五");
            entity.setK(ConstantCode.BASE_CONFIG_THEME_K_FIVE);
            entity.setV(ConstantCode.BASE_CONFIG_THEME_V_FIVE);
            entity.setEnabled(false);
            this.baseMapper.insert(entity);
        }
        // 系统主题六
        Integer sixCount = this.baseMapper.selectCount(new QueryWrapper<BaseConfigEntity>()
                .eq("k", ConstantCode.BASE_CONFIG_THEME_K_SIX)
                .eq("v", ConstantCode.BASE_CONFIG_THEME_V_SIX));
        if(sixCount == null || sixCount == 0) {
            BaseConfigEntity entity = new BaseConfigEntity();
            entity.setId(UuidUtils.getUniqueIdByUUId());
            entity.setCategory(ConstantCode.BASE_CONFIG_THEME_TYPE);
            entity.setName("主题六");
            entity.setK(ConstantCode.BASE_CONFIG_THEME_K_SIX);
            entity.setV(ConstantCode.BASE_CONFIG_THEME_V_SIX);
            entity.setEnabled(false);
            this.baseMapper.insert(entity);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public ResultUtil<JSONObject> getInitData() {
        JSONObject object = new JSONObject();
        // 获取系统主题参数配置
        BaseConfigEntity configEntity = this.baseMapper.selectOne(new QueryWrapper<BaseConfigEntity>()
                .eq("category", ConstantCode.BASE_CONFIG_THEME_TYPE)
                .eq("enabled",1));
        object.put("theme",configEntity);
        return ResultUtil.success(object);
    }

    @Override
    @Transactional(readOnly = true)
    public ResultUtil<List<BaseConfigEntity>> queryThemeList() {
        List<BaseConfigEntity> entityList = this.baseMapper.selectList(new QueryWrapper<BaseConfigEntity>().eq("category", ConstantCode.BASE_CONFIG_THEME_TYPE));
        return ResultUtil.success(entityList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<BaseConfigEntity> updateThemeEnabled(String id) {
        BaseConfigEntity configEntity = this.baseMapper.selectOne(new QueryWrapper<BaseConfigEntity>()
                .eq("category", ConstantCode.BASE_CONFIG_THEME_TYPE)
                .eq("enabled",1));
        if(configEntity.getId().equals(id)) {
            return ResultUtil.error("该主题为当前系统使用主题，不能切换");
        }else {
            // 更新主题启用模式
            configEntity.setEnabled(false);
            this.baseMapper.updateById(configEntity);
            // 切换其它主题
            BaseConfigEntity newEntity = this.baseMapper.selectById(id);
            newEntity.setEnabled(true);
            this.baseMapper.updateById(newEntity);
            return ResultUtil.success(newEntity);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public ResultUtil<List<Map<String, String>>> listCategory() {
        List<Map<String, String>> configData = ConfigConstantCode.getInstance().getConfigData();
        return ResultUtil.success(configData);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<BaseConfigEntity> saveConfig(BaseConfigEntity request) {
        request.setId(UuidUtils.getUniqueIdByUUId());
        int insert = this.baseMapper.insert(request);
        return insert > 0 ? ResultUtil.success() : ResultUtil.error();
    }

    @Override
    @Transactional(readOnly = true)
    public ResultUtil<List<BaseConfigEntity>> queryList(BaseConfigEntity request) {
        List<BaseConfigEntity> entityList = this.baseMapper.selectList(new QueryWrapper<BaseConfigEntity>().eq("category", request.getCategory()));
        return ResultUtil.success(entityList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<BaseConfigEntity> updateConfig(BaseConfigEntity request) {
        int update = this.baseMapper.updateById(request);
        return update > 0 ? ResultUtil.success() : ResultUtil.error();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<BaseConfigEntity> deleteConfig(String id) {
        int delete = this.baseMapper.deleteById(id);
        return delete > 0 ? ResultUtil.success() : ResultUtil.error();
    }
}
