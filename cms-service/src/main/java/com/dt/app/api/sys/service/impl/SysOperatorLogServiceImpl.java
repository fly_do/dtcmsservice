package com.dt.app.api.sys.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.app.api.sys.mapper.SysOperatorLogMapper;
import com.dt.app.api.sys.service.SysOperatorLogService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.modules.sys.entities.SysOperatorLogEntity;
import com.dt.app.modules.sys.vo.request.SysOperatorLogPageRequest;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 系统操作日志服务实现类
 * @author DT
 * @date 2021/11/20 17:37
 */
@CommonsLog
@Service
public class SysOperatorLogServiceImpl extends ServiceImpl<SysOperatorLogMapper, SysOperatorLogEntity> implements SysOperatorLogService {

    @Async("sysTaskExecutor")
    @Override
    public void saveOperatorLog(SysOperatorLogEntity sysLog) {
        this.baseMapper.insert(sysLog);
        log.info("===============操作日志成功写入数据库===============");
    }

    @Override
    @Transactional(readOnly = true)
    public ResultUtil<IPage<SysOperatorLogEntity>> queryOperatorLogList(SysOperatorLogPageRequest request) {
        Page<SysOperatorLogEntity> page = new Page<>(request.getCurrent(),request.getSize());
        IPage<SysOperatorLogEntity> list = this.baseMapper.queryList(page,request);
        return ResultUtil.success(list);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultUtil<?> deleteOperatorLogById(String id) {
        int delete = this.baseMapper.deleteById(id);
        return delete > 0 ? ResultUtil.success() : ResultUtil.error();
    }

}
