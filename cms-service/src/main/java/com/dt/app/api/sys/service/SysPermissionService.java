package com.dt.app.api.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.modules.sys.entities.SysPermissionEntity;
import com.dt.app.modules.sys.vo.request.SysMenuRequest;
import com.dt.app.modules.sys.vo.request.SysMenuSaveRequest;
import com.dt.app.modules.sys.vo.response.SysMenuTreeResponse;

import java.util.List;

/**
 * 系统权限服务接口
 * @author DT
 * @date 2021/6/2 22:19
 */
public interface SysPermissionService extends IService<SysPermissionEntity> {

    List<SysPermissionEntity> getPermissionListByUserId(Long userId);

    ResultUtil<List<SysMenuTreeResponse>> queryList(SysMenuRequest request);

    ResultUtil<SysMenuSaveRequest> saveMenu(SysMenuSaveRequest request);

    ResultUtil<SysPermissionEntity> getMenuById(Long id);

    ResultUtil<SysMenuSaveRequest> updateMenu(SysMenuSaveRequest request);

    ResultUtil<SysPermissionEntity> deleteMenu(Long id);

    List<SysPermissionEntity> getPermissionListByRoleId(Integer roleId);

    ResultUtil<List<SysMenuTreeResponse>> listTree();
}
