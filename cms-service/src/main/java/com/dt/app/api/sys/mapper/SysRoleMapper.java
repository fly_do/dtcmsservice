package com.dt.app.api.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dt.app.modules.sys.entities.SysRoleEntity;

/**
 * 系统角色数据接口
 * @author DT
 * @date 2021/6/2 22:24
 */
public interface SysRoleMapper extends BaseMapper<SysRoleEntity> {

}
