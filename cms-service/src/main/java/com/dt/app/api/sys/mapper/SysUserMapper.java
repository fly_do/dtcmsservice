package com.dt.app.api.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dt.app.modules.sys.entities.SysUserEntity;
import com.dt.app.modules.sys.vo.request.SysUserPageRequest;
import com.dt.app.modules.sys.vo.response.SysUserPageResponse;
import com.dt.app.modules.sys.vo.response.SysUserResponse;

/**
 * 系统用户数据接口
 * @author DT
 * @date 2021/6/2 22:24
 */
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

    IPage<SysUserPageResponse> queryList(Page<SysUserPageResponse> page, SysUserPageRequest request);

    SysUserResponse selectUserById(Long id);

    int updatePwdById(String newPassword, Long userId);
}
