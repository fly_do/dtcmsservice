package com.dt.app.api.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dt.app.modules.sys.entities.SysRolePermissionEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统角色关联权限数据接口
 * @author DT
 * @date 2021/6/2 22:24
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermissionEntity> {

    void saveRolePermissions(@Param("roleId") Long roleId, @Param("perIds") List<Long> ids);
}
