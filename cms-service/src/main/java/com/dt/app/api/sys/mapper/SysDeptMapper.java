package com.dt.app.api.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dt.app.modules.sys.entities.SysDepartmentEntity;
import com.dt.app.modules.sys.vo.request.SysDeptRequest;
import com.dt.app.modules.sys.vo.response.SysDeptTreeResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统部门数据接口
 * @author DT
 * @date 2021/6/13 12:03
 */
public interface SysDeptMapper extends BaseMapper<SysDepartmentEntity> {

    List<SysDeptTreeResponse> queryList(@Param("request") SysDeptRequest request);

}
