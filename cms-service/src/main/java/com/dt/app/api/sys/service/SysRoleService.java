package com.dt.app.api.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dt.app.common.response.ResultUtil;
import com.dt.app.modules.sys.entities.SysRoleEntity;
import com.dt.app.modules.sys.vo.request.SysRoleRequest;
import com.dt.app.modules.sys.vo.request.SysRoleSaveRequest;
import com.dt.app.modules.sys.vo.response.SysRoleResponse;

import java.util.List;

/**
 * 系统角色服务接口
 * @author DT
 * @date 2021/6/2 22:19
 */
public interface SysRoleService extends IService<SysRoleEntity> {

    ResultUtil<List<SysRoleResponse>> queryList(SysRoleRequest request);

    ResultUtil<SysRoleSaveRequest> saveRole(SysRoleSaveRequest request);

    ResultUtil<SysRoleEntity> getRoleById(Integer id);

    ResultUtil<SysRoleSaveRequest> updateRole(SysRoleSaveRequest request);

    ResultUtil<SysRoleEntity> delete(Integer id);

    ResultUtil<List<SysRoleEntity>> findAll();
}
