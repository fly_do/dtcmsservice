package com.dt.app.api.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dt.app.modules.sys.entities.SysUserRoleEntity;

/**
 * 系统用户关联角色数据接口
 * @author DT
 * @date 2021/6/2 22:24
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRoleEntity> {

}
