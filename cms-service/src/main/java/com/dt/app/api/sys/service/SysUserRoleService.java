package com.dt.app.api.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dt.app.modules.sys.entities.SysUserRoleEntity;

/**
 * 系统用户关联角色服务接口
 * @author DT
 * @date 2021/6/2 22:19
 */
public interface SysUserRoleService extends IService<SysUserRoleEntity> {

}
