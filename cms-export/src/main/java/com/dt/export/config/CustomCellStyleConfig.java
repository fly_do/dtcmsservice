package com.dt.export.config;

import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import org.apache.poi.ss.usermodel.*;

/**
 * 自定义表格样式
 * @author DT
 * @date 2021/10/24 2:11
 */
public class CustomCellStyleConfig {

    public static HorizontalCellStyleStrategy customStyle() {
        // 头的样式
        WriteCellStyle headStyle = new WriteCellStyle();
        headStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        // 设置颜色填充
        headStyle.setFillPatternType(FillPatternType.SOLID_FOREGROUND);
        WriteFont headFont = new WriteFont();
        // 字体加粗
        headFont.setBold(true);
        headFont.setFontHeightInPoints((short) 11);
        // 表头字体颜色
        headFont.setColor(IndexedColors.WHITE.getIndex());
        headStyle.setWriteFont(headFont);
        headStyle.setWrapped(true);
        //headStyle.setBorderLeft(BorderStyle.NONE);
        //headStyle.setBorderRight(BorderStyle.NONE);
        // 内容的样式
        WriteCellStyle contentStyle = new WriteCellStyle();
        // 填充颜色
        contentStyle.setFillPatternType(FillPatternType.NO_FILL);
        contentStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        WriteFont contentWriteFont = new WriteFont();
        contentWriteFont.setFontName("宋体");
        contentWriteFont.setFontHeightInPoints((short) 11);
        contentStyle.setWriteFont(contentWriteFont);
        contentStyle.setBorderTop(BorderStyle.THIN);
        contentStyle.setBorderBottom(BorderStyle.THIN);
        contentStyle.setBorderLeft(BorderStyle.THIN);
        contentStyle.setBorderRight(BorderStyle.THIN);
        // 水平居中
        contentStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
        // 垂直居中
        contentStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        // 自动换行
        contentStyle.setWrapped(true);
        return new HorizontalCellStyleStrategy(headStyle, contentStyle);
    }

}
