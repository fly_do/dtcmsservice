package com.dt.export.data;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 导出用户表
 * @author DT
 * @date 2021/10/23 19:58
 */
@Data
@ColumnWidth(25)
public class SysUserData implements Serializable {

    private static final long serialVersionUID = 4560271875314902351L;
    @ExcelIgnore
    private Long id;

    @ExcelProperty(value = {"登录账号"})
    private String username;

    @ExcelProperty(value = {"用户名"})
    private String accountName;

    @ExcelProperty(value = {"邮箱"})
    private String email;

    @ExcelProperty(value = {"部门名称"})
    private String deptName;

    @ExcelProperty(value = {"管理员"})
    private String isAdmin;

    @DateTimeFormat("yyyy-MM-dd")
    @ExcelProperty(value = "创建时间")
    private Date createTime;

}
